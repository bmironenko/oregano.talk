"""Bootstrap project development. Install appropriate dependencies, etc.
"""

from __future__ import absolute_import, print_function
import os
import subprocess
import json
import re
import sys

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'get_dependencies'
]

_pyversion = '{0}.{1}'.format(*sys.version_info[:2])
_conf_file = 'bootstrap.json'
_pip_index_urls = ['http://pypi.python.org/simple/']
_virtualenv_path = '.virtualenv'


def _load_config(name=_conf_file):
    """Load bootstrap configuration from file.

    :param str name: configuration file name.
    :return: configuration dictionary.
    """
    try:
        conf_file_path = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            name)
        with open(conf_file_path, 'r') as f:
            return json.load(f)
    except Exception as ex:
        print('Error loading configuration from {name}: "{ex}"'.format(
            name=name, ex=str(ex)))


def _get_submodules():
    """Return the list of git submodules for this project.

    :return: list of submodule names.
    """
    gs = subprocess.Popen(('git', 'submodule'), stdout=subprocess.PIPE)
    output = subprocess.check_output(('awk', '{print $2}'), stdin=gs.stdout)
    gs.wait()  
    output = output.decode('utf-8')
    return [line for line in output.split('\n') if line]


def get_dependencies(kind, recurse=False, config=None):
    """Return the list of dependency packages for this version of Python.

    :param str kind: type of dependencies, i.e. "install" or "setup".
    :param bool recurse: recurse into submodules and include their \
        dependencies, but exclude submodules themselves.
    :param dict config: dependency configuration (bootstrap.json); \
        if :const:`None`, load from the default configuration file.
    :return: a generator object yielding dependencies.
    """
    if config is None:
        config = _load_config()

    if recurse:
        submodules = _get_submodules()
        for sm in submodules:
            sm_conf_file = os.path.join(sm, _conf_file)
            if os.path.exists(sm_conf_file):
                sm_conf = _load_config(sm_conf_file)
                cwd = os.getcwd()
                os.chdir(os.path.join(cwd, sm))
                for package in get_dependencies(kind, recurse, sm_conf):
                    yield package
                os.chdir(cwd)

    for version_regex, spec in config.get('dependencies', dict()).items():
        version_regex = re.sub(r'\\(.)', r'\1', version_regex)  # unescape RE
        if re.match(version_regex, _pyversion):
            for package in spec.get(kind, list()):
                if not recurse or not package in submodules:
                    yield package


def _install_dependencies(kind, config):
    """Install packages.

    :param str kind: type of dependencies, i.e. "install" or "setup".
    :param dict config: dependency configuration (bootstrap.json).
    """
    from pip.index import PackageFinder
    from pip.req import InstallRequirement, RequirementSet
    from pip.locations import build_prefix, src_prefix

    # Build the requirement set.
    requirement_set = RequirementSet(
        build_dir=build_prefix,
        src_dir=src_prefix,
        download_dir=None
    )
    for package in get_dependencies(kind, recurse=True, config=config):
        requirement_name = InstallRequirement.from_line(package)
        if not requirement_set.has_requirement(requirement_name.name):
            print(package)
            requirement_set.add_requirement(requirement_name)

    # Install packages.
    finder = PackageFinder(find_links=[], index_urls=_pip_index_urls)
    requirement_set.prepare_files(finder, force_root_egg_info=False,
                                  bundle=False)
    requirement_set.install([])

if __name__ == '__main__':
    config = _load_config()
    print('Installing setup dependencies...')
    _install_dependencies('setup', config)
    print('\nInstalling production dependencies...')
    _install_dependencies('install', config)
