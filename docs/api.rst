==============
 API
==============

oregano.talk.interfaces
=======================

.. automodule:: oregano.talk.interfaces

Transports
----------

.. class:: IConnection

    Generic connection interface. Marks an object that provides connection
    semantics. If a transport instance provides :class:`IConnection`,
    for example, any channel using the transport will invoke its
    :func:`connect()` method prior to using it.

    .. attribute:: connected

        Indicates the state of the connection (:const:`True` or :const:`False`).

    .. method:: connect

        Establish the connection.

    .. method:: close

        Close the connection.

.. class:: IFileDescriptor

    Transports needing to expose their underlying file descriptor should
    implement this interface.

    .. attribute:: fileno

        File descriptor value (:py:class:`int`).

.. class:: ITransport

    Transport interface. Defines low-level read/write access.

    .. attribute:: conf

        :class:`ITransportConf` instance that configures this transport.

    .. method:: write(data)

        Write data to the transport.

        :param bytes data: data to write (must implement the buffer protocol).
        :return: number of bytes written.

    .. method:: read([count])

        Read data from the transport.

        :param int count: number of bytes to read. Blocking transport reads \
            will block until timed out (if read timeout is configured) or the \
            amount of data specified is available. When :const:`None`, return \
            whatever data is available.
        :return: data received (buffer), or :const:`None` if no data is \
            available.

    .. method:: read_into(buffer):

        Read data from the transport into a buffer.

        :param bytes buffer: byte array to populate instead of returning data. \
            This can be a memoryview created with an offset, for example. The \
            length of the buffer specifies the number of bytes to read. \
            Blocking transport reads will block until timed out (if read \
            timeout is configured) or the amount of data specified is \
            available. When :const:`None`, return whatever data is available.
        :return: number of bytes stored in the buffer.

.. class:: ITransportConf

    Interface for classes that provide generic transport configuration.

    .. attribute:: blocking

        Set or return blocking mode (:const:`True` or :const:`False`).

    .. attribute:: write_timeout

        Write timeout, in seconds, for blocking mode.

    .. attribute:: read_timeout

        Read timeout, in seconds, for blocking mode.

.. class:: ITransportConfFactory

    Interface for registering :class:`ITransportConf` factories.

Channels
--------

.. class:: IChannel

    Message channel. Handles message boundaries. Does not implement
    request-response semantics; that is the job of the higher :class:`ISession`
    layer. Channel behavior depends on whether the underlying transport is
    configured in non-blocking mode. When it is, :func:`send()` and
    :func:`receive()` are asynchronous and return
    :class:`concurrent.futures.Future` instances.

    .. attribute:: conf

        :class:`IChannelConf` instance that configures this channel.

    .. method:: send(message):

        Send a message, wrapping it in an envelope/header.

        :param bytes message: message to send.
        :return: if the underlying transport is non-blocking, a \
            :py:class:`concurrent.futures.Future` object. If the transport is \
            blocking, :const:`True` if the message was successfully sent and \
            :const:`False` otherwise (though, in this case, an exception will \
            most likely be raised).

    .. method:: receive():

        Receive the next message.

        :return: message received, or a :py:class:`concurrent.futures.Future` \
            instance if the underlying transport is non-blocking.

.. class:: IChannelConf

    Interface for ChannelConf classes.

    .. attribute:: transport_conf

        :class:`ITransportConf` instance specifying the transport to use.

    .. attribute:: message_envelope

        :class:`IMessageEnvelope` instance for wrapping/unwrapping messages.

    .. attribute:: receive_timeout

        Return or set the message receive timeout, in seconds.


.. class:: IChannelConfFactory

    Interface for registering :class:`IChannelConf` factories.


Message envelopes
-----------------

.. class:: IMessageEnvelope

    Message envelope. Knows how to wrap a message prior to sending it; knows
    how to recognize a message when receiving byte data.

    .. method:: wrap(message):

        Wrap the message prior to sending it.

        :param bytes message: message to wrap.
        :return: wrapped message data, ready to be sent.

    .. method:: unwrap():

        Receive a message. This is a generator that yields instructions to \
        the client. Each instruction is a *(more, what)* tuple, and can be \
        one of:

        - (:const:`True`, :py:class:`int`): caller should read this many bytes;
        - (:const:`True`, :py:class:`bytes`): caller should read until the \
          specified byte sequence;
        - (:const:`False`, :py:class:`bytes`): done; complete message is \
          :py:class:`bytes`; next call to the generator will result in a \
          :py:class:`StopIteration`.

        Caller should send the sequence of bytes read back to the generator.

.. class:: IMessageEnvelopeFactory

    Interface for registering :class:`IMessageEnvelope` factories as a
    zope.component utility.

Helpers
-------

.. autofunction:: create_channel_conf
.. autofunction:: create_transport_conf
.. autofunction:: create_message_envelope

oregano.talk.exceptions_
========================

.. automodule:: oregano.talk.exceptions_

.. autoexception:: _BaseError
    :members:

    .. automethod:: __init__

.. autoexception:: MessageEnvelopeError

.. autoexception:: TransportError

oregano.talk.warnings
=====================

.. automodule:: oregano.talk.warnings

.. autoclass:: TransportConfWarning

oregano.talk.transports.serial
==============================

.. automodule:: oregano.talk.transports.serial

.. autoclass:: SerialTransportConf
    :members:

    .. automethod:: __init__

.. autoclass:: SerialTransport
    :members:

    .. automethod:: __init__

oregano.talk.envelopes.header
=============================

.. automodule:: oregano.talk.envelopes.header

.. autoclass:: MessageHeader
    :members:

    .. automethod:: __init__

oregano.talk.envelopes.ant
==========================

.. automodule:: oregano.talk.envelopes.ant

.. autoclass:: ANTEnvelope
    :members:

    .. automethod:: __init__

oregano.talk.channels.tornado
=============================

.. automodule:: oregano.talk.channels.tornado

.. autoclass:: TransportStream
    :members:

    .. automethod:: __init__

.. autoclass:: StreamChannel
    :members:

    .. automethod:: __init__

.. autoclass:: TornadoChannelConf
    :members:

    .. automethod:: __init__

.. autoclass:: TornadoChannel
    :members:

    .. automethod:: __init__

oregano.talk.channels.synchronous
=================================

.. automodule:: oregano.talk.channels.synchronous

.. autoclass:: SynchronousChannelConf
    :members:

    .. automethod:: __init__

.. autoclass:: SynchronousChannel
    :members:

    .. automethod:: __init__

.. _`zope.component`: http://docs.zope.org/zope.component