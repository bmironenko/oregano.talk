==============
 Examples
==============

.. module:: examples

Registering a TransportConf factory

.. code-block:: python

    from zope.component.factory import Factory
    from zope.component import getGlobalSiteManager
    gsm = getGlobalSiteManager()
    gsm.registerUtility(Factory(SerialTransportConf, 'SerialTransportConf'), ITransportConfFactory, 'serial')