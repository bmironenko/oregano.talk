.. oregano.talk documentation master file, created by
   sphinx-quickstart on Thu Sep 12 22:26:21 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to oregano.talk's documentation!
========================================

This module aims to provide the tools for developers to build communication
protocols for microcontrollers (such as Arduino), though it most certainly
is not limited to that. It currently provides non-blocking serial communication
with message semantics out of the box using a Tornado IO loop, making
high-performance REST API or web application development a snap.

At its core, oregano.talk is a framework that defines transports, channels, and
protocols. Some implementations of these (e.g. serial transport, asynchronous
Tornado-based channel, etc) are included, but the user can easily create their
own plugin components if needed.


Contents:

.. toctree::
    :maxdepth: 2

    overview
    examples
    api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

