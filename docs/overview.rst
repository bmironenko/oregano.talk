==============
 Overview
==============

.. module:: overview


This module aims to provide the tools for developers to build communication
protocols for microcontrollers (such as Arduino), though it most certainly
is not limited to that. It currently provides non-blocking serial communication
with message semantics out of the box using a Tornado IO loop, making
high-performance REST API or web application development a snap.

At its core, oregano.talk is a framework that defines transports, channels, and
protocols. Some implementations of these (e.g. serial transport, asynchronous
Tornado-based channel, etc) are included, but the user can easily create their
own plugin components if needed.

Copyright (C) 2013 Basil Mironenko <bmironenko(at)me.com>

Requirements
============

Python 2.7 or 3.3 is required to run; other versions may work but have not been
tested with.

The following Python packages are direct requirements:

- `zope.interface`_
- `zope.component`_
- `Tornado`_
- `pySerial`_

.. _`zope.interface`: https://pypi.python.org/pypi/zope.interface/4.0.5
.. _`zope.component`: https://pypi.python.org/pypi/zope.component/4.1.0
.. _`tornado`: https://pypi.python.org/pypi/tornado/3.1.1
.. _`pySerial`: https://pypi.python.org/pypi/pyserial/2.6

Installation
============

The project follows the `git flow`_ workflow for branch management.
Consequently, you will need to check out the ``master`` branch for the latest
release, or ``develop`` for the current development "trunk".

    | ``git clone git@bitbucket.org:bmironenko/oregano.talk.git``
    | ``cd oregano.talk``
    | ``git checkout develop``
    | ``python setup.py install``

Note that running ``setup.py`` commands will also install packages required for
internal tests:

- `nose`_
- `coverage`_
- `nosexcover`_

Tests
=====

The internal test suite can be executed to verify your environment:

    | ``python setup.py test``

    or

    | ``python setup.py nosetests``

Resources
=========

- `oregano.talk on BitBucket`_


.. _`nose`: https://pypi.python.org/pypi/nose/1.3.0
.. _`coverage`: https://pypi.python.org/pypi/coverage/3.6
.. _`nosexcover`: https://pypi.python.org/pypi/nosexcover/1.0.8
.. _`git flow`: http://nvie.com/posts/a-successful-git-branching-model/
.. _`oregano.talk on BitBucket`: https://bitbucket.org/bmironenko/oregano.talk
.. _`repo`: https://bitbucket.org/bmironenko/oregano.talk
