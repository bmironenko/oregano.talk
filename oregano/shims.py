"""Compatibility shims.
"""

from __future__ import absolute_import, print_function
import sys

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'b'
]

if sys.version_info[0] < 3:
    b = lambda d, f: bytes(d)
else:
    b = bytes
