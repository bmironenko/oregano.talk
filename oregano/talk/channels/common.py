"""Common channel functionality.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'RingBuffer',
    'ChannelConf',
]

from collections import deque
import logging
from zope.interface import implementer
from ..interfaces import *


class RingBuffer(object):
    """Implements a ring buffer using a deque.
    """
    _logger = logging.getLogger(__name__ + '.RingBuffer')

    def __init__(self, max_len):
        """Initialize a new RingBuffer instance.

        :param int max_len: maximum buffer size.
        """
        if not isinstance(max_len, int) or max_len <= 0:
            error = 'Invalid maximum buffer size'
            self._logger.error(error)
            raise ValueError(error)
        self._logger.debug('Creating with: size=%r', max_len)
        self._dq = deque()
        self.max_len = max_len

    def __len__(self):
        """Return the current length of the buffer.
        """
        return sum(len(chunk) for chunk in self._dq)

    @property
    def available(self):
        """Return the space available in the buffer (in bytes).
        """
        return self.max_len - len(self)

    def enqueue(self, chunk):
        """Add specified data to the tail of the buffer.

        :param bytes chunk: byte sequence to insert at the tail of the buffer.
        """
        error = None
        if not isinstance(chunk, (bytes, bytearray, memoryview)):
            error = '\'chunk\' must implement the buffer protocol'
        else:
            if len(chunk) > self.available:
                error = 'Chunk ({size} bytes) is larger than buffer ' \
                        'space available ({avail})'.format(
                            size=len(chunk),
                            avail=self.available)
        if error:
            self._logger.error(error)
            raise ValueError(error)

        self._logger.debug('Enqueueing a %r byte chunk', len(chunk))
        self._dq.append(chunk)

    def dequeue(self, count=None):
        """Remove up to a number of bytes from the head of the buffer.

        :param int count: number of bytes to remove; when :const:`None`, \
            dequeue all available data.
        :return: a :class:`bytes` instance containing buffered data up to \
            the number of specified bytes.
        """
        return self._get(count, pop=True)

    def peek(self, count=None):
        """Return up to the specified number of bytes without removing them
        from the buffer.

        :param int count: number of bytes to return; when :const:`None`, \
            return all available data.
        :return: a :class:`bytes` instance containing buffered data up to \
            the number of specified bytes.
        """
        return self._get(count, pop=False)

    def find(self, delimiter):
        """Find a byte sequence in the buffer. Since the delimiter may
        straddle multiple chunks, merge them in the process.

        :param bytes delimiter: byte sequence (delimiter) to find.
        :return: delimiter position within the buffer, or -1 if not found.
        """
        if not isinstance(delimiter, (bytes, bytearray, memoryview)):
            error = '\'delimiter\' must provide the buffer interface'
            self._logger.error(error)
            raise ValueError(error)

        if not self._dq:
            return -1

        pos = self._dq[0].find(delimiter)
        if pos > -1:
            # Delimiter was found in the first chunk.
            return pos
        elif len(self._dq) == 1:
            return -1
        else:
            # Merge with the next chunk and repeat. This is technically an
            # expensive operation but we wouldn't normally see it unless we
            # get A LOT of junk input.
            self._dq.appendleft(self._dq.popleft() + self._dq.popleft())
            return self.find(delimiter)

    def _get(self, count=None, pop=True):
        """Return up to the specified number of bytes without removing them
        from the buffer.

        :param int count: number of bytes to return; when :const:`None`, \
            return all available data.
        :param bool pop: if :const:`True`, removed any bytes returned from \
            the buffer.
        :return: a :class:`bytes` instance containing buffered data up to \
            the number of specified bytes.
        """
        if count is None:
            count = len(self)
        elif not isinstance(count, int) or count < 0:
            error = 'Invalid count'
            self._logger.error(error)
            raise ValueError(error)

        self._logger.debug('%s %r bytes from the buffer',
                           'Dequeueing' if pop else 'Peeking', count)

        data, pos, idx = bytearray(count), 0, 0
        while self._dq and idx < len(self._dq):
            chunk_view = memoryview(self._dq[idx])

            # Useable chunk is the number of bytes we still need to get.
            useable_chunk = chunk_view[:count]
            # Remaining chunk is what would remain of this chunk once we get
            # the bytes we need.
            remaining_chunk = chunk_view[count:]

            if not remaining_chunk:
                # If there isn't anything left, grab the entire chunk.
                if pop:
                    data[pos:pos+len(useable_chunk)] = self._dq.popleft()
                else:
                    data[pos:pos+len(useable_chunk)] = self._dq[idx]
                    idx += 1
            else:
                # Otherwise use only the useable chunk and replace it with
                # the remainder. We can't really use a memoryview since the
                # new chunk will need to support find().
                data[pos:pos+len(useable_chunk)] = useable_chunk
                if pop:
                    self._dq[idx] = bytearray(remaining_chunk)

            # Update the remaining count and see if we are done.
            pos += len(useable_chunk)
            count -= len(useable_chunk)
            if count <= 0:
                break

        # Remove extra unfilled space from the return value.
        if count > 0:
            del data[-count:]
        return data


@implementer(IChannelConf)
class ChannelConf(object):
    """Generic channel configuration object.
    """
    _logger = logging.getLogger(__name__ + '.ChannelConf')

    def __init__(self, transport_conf, message_envelope, receive_timeout=None):
        """Initialize a new ChannelConf instance.

        :param oregano.talk.interfaces.ITransportConf transport_conf:
            transport configuration.
        :param oregano.talk.interfaces.IMessageEnvelope message_envelope:
            message envelope.
        :param receive_timeout: timeout for receiving messages, in seconds.
        :exception ValueError: `transport_conf` does not provide \
            :class:`oregano.talk.interfaces.ITransportConf`,
            `message_envelope` does not provide \
            :class:`oregano.talk.interfaces.IMessageEnvelope`, or an invalid \
            `receive_timeout` is specified.
        """
        self._logger = logging.getLogger(
            __name__ + '.' + self.__class__.__name__)
        self._logger.debug('Creating with: transport_conf=%r, '
                           'message_envelope=%r, '
                           'receive_timeout=%r',
                           transport_conf, message_envelope, receive_timeout)

        error = None
        if not ITransportConf.providedBy(transport_conf):
            error = 'Transport configuration object must provide ' \
                    'ITransportConf'
        elif not IMessageEnvelope.providedBy(message_envelope):
            error = 'Message envelope object must provide IMessageEnvelope'
        elif receive_timeout is not None and receive_timeout < 0:
            error = 'Invalid receive timeout value'
        if error:
            self._logger.error(error)
            raise ValueError(error)

        self._transport_conf = transport_conf
        self._message_envelope = message_envelope
        self._receive_timeout = receive_timeout

    @property
    def transport_conf(self):
        """Return the transport configuration object.
        """
        return self._transport_conf

    @property
    def message_envelope(self):
        """Return the message envelope object.
        """
        return self._message_envelope

    @property
    def receive_timeout(self):
        """Return the message receive timeout (when blocking), in seconds.
        """
        return self._receive_timeout

    def __repr__(self):  # pragma: no cover
        """Return a string representation of this configuration object.
        """
        return '{{transport_conf={!r}, message_envelope={!r}, ' \
               'receive_timeout={!r}}}'.format(
               self.transport_conf, self.message_envelope, self.receive_timeout)
