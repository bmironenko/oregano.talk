"""Basic synchronous channel.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'SynchronousChannelConf',
    'SynchronousChannel'
]

import logging
import time
from zope.interface import implementer, Attribute
from zope.component import getGlobalSiteManager, adapter
from zope.component.factory import Factory
from .common import ChannelConf, RingBuffer
from ..interfaces import *
from ..exceptions_ import ChannelTimeoutError


class _ISynchronousChannelConf(IChannelConf):
    """Marker interface for the :class:`SynchronousChannelConf` class; exists
    for adapter registration purposes.
    """
    receive_buffer_size = Attribute(
        'Read buffer size',
        'Maximum size of the channel receive buffer.')


@implementer(_ISynchronousChannelConf)
class SynchronousChannelConf(ChannelConf):
    """Configuration for the synchronous message channel.
    """
    def __init__(self, transport_conf, message_envelope,
                 receive_timeout=None, receive_buffer_size=2048):
        """Initialize a new TornadoChannelConf instance.

        :param oregano.talk.interfaces.ITransportConf transport_conf:
            transport configuration.
        :param oregano.talk.interfaces.IMessageEnvelope message_envelope:
            message envelope.
        :param receive_timeout: timeout for receiving messages, in seconds.
        :param receive_buffer_size: maximum size of the read buffer.
        :exception ValueError: `transport_conf` specifies a non-blocking \
            transport, or an invalid `receive_buffer_size` is specified.
        """
        super(SynchronousChannelConf, self).__init__(transport_conf,
                                                     message_envelope,
                                                     receive_timeout)
        error = None
        if not transport_conf.blocking:
            error = 'Must use a blocking transport'
        elif not isinstance(receive_buffer_size, int) or \
                receive_buffer_size <= 0:
            error = 'Invalid read buffer size'
        if error:
            self._logger.error(error)
            raise ValueError(error)

        self._receive_buffer_size = receive_buffer_size

    @property
    def receive_buffer_size(self):
        """Return the receive buffer size, in bytes.
        """
        return self._receive_buffer_size


@implementer(IChannel)
@adapter(_ISynchronousChannelConf)
class SynchronousChannel(object):
    """Implements a basic synchronous (blocking) channel.
    """
    _READ_SLEEP_INTERVAL = 0.1
    _logger = logging.getLogger(__name__ + '.SynchronousChannel')

    def __init__(self, conf):
        """Initialize a new SynchronousChannel instance.

        :param oregano.talk.channels.synchronous.SynchronousChannelConf conf: \
            channel configuration object.
        :exception ValueError: `conf` is not an instance of \
            :class:`SynchronousChannelConf`.
        """
        self._logger.debug('Creating with: conf=%r', conf)
        if not isinstance(conf, SynchronousChannelConf):
            error = 'Configuration object must be an instance of ' \
                    'SynchronousChannelConf'
            self._logger.error(error)
            raise ValueError(error)
        self._conf = conf
        self._transport = ITransport(conf.transport_conf)
        self._is_connection_transport = IConnection.providedBy(self._transport)
        if self._is_connection_transport and not self._transport.connected:
            self._transport.connect()

        # Reads are buffered. When looking for specific tokens in the stream,
        # we will read all available data and save any "leftovers" (data past
        # the token, if found) for subsequent reads.
        self._receive_buffer = RingBuffer(conf.receive_buffer_size)

    @property
    def conf(self):
        """Return the channel configuration object.
        """
        return self._conf

    def _read_into_buffer(self, count=None):
        """Read data from the transport into the buffer.

        :param int count: number of bytes to read; if :const:`None`, read \
            whatever data is available without blocking.
        :return: number of bytes read.
        """
        self._logger.debug('Reading %r bytes into the buffer',
                           count if count is not None else 'available')
        data = self._transport.read(count=count)
        if data:
            self._receive_buffer.enqueue(data)
        return len(data) if data is not None else 0

    def _read(self, count=None):
        """Read available data. Buffer transport reads.

        :param int count: number of bytes to read; if :const:`None`, read
            whatever data is available.
        :return: bytes read as a :class:`memoryview` instance.
        """
        self._logger.debug('Reading %r bytes',
                           count if count is not None else 'available')

        # Buffer any available data immediately.
        self._read_into_buffer()

        if count is None:
            return self._receive_buffer.dequeue()
        else:
            data = bytearray(count)
            available_data = self._receive_buffer.dequeue(count)
            available = len(available_data)
            data[:available] = available_data
            if available == count:
                return data
            else:
                # There is more to read.
                count -= available
                self._read_into_buffer(count)
                more = self._receive_buffer.dequeue(count)
                data[available:available+len(more)] = more
            return data

    def _read_until(self, delimiter):
        """Read from the transport until data is encountered.

        :param bytes delimiter: read until the delimiter is encountered.
        :return: all of the data read up to and including the delimiter; \
            :const:`None` if the delimiter was not received.
        """
        time_waited = 0.0

        while not self.conf.receive_timeout or \
                time_waited < self.conf.receive_timeout:
            self._read_into_buffer()
            if self._receive_buffer:
                position = self._receive_buffer.find(delimiter)
                if position > -1:
                    return self._receive_buffer.dequeue(position + len(delimiter))
            else:
                time.sleep(self._READ_SLEEP_INTERVAL)
                if self.conf.receive_timeout:
                    time_waited += self._READ_SLEEP_INTERVAL
        else:
            # Timed out.
            raise ChannelTimeoutError('Timed out while waiting for '
                                      '{delim!r}'.format(delim=delimiter))

    def send(self, message):
        """Send a message, wrapping it in an envelope/header.

        :param bytes message: message to send.
        :return: :const:`True` if the message was successfully sent and \
            :const:`False` otherwise (though, in this case, an exception will \
            most likely be raised).
        """
        self._transport.write(self.conf.message_envelope.wrap(message))
        return True

    def receive(self):
        """Receive the next message.

        :return: message received.
        """
        gen = self.conf.message_envelope.unwrap()
        more, bytes_ = next(gen)
        while more:
            if isinstance(bytes_, bytes):
                data = self._read_until(bytes_)
            else:
                data = self._read(count=bytes_)
            more, bytes_ = gen.send(data)
        return bytes_


gsm = getGlobalSiteManager()
gsm.registerUtility(Factory(SynchronousChannelConf, 'SynchronousChannelConf'),
                    IChannelConfFactory, 'synchronous')
gsm.registerAdapter(SynchronousChannel, (_ISynchronousChannelConf,),
                    IChannel, '')

