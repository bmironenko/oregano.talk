"""Tests for common channel functionality.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import re
from zope.interface import directlyProvides
from zope.interface.verify import verifyObject
from ..common import *
from ....test import *
from ....talk import interfaces


class RingBufferTest(unittest.TestCase):
    """Test the RingBuffer class.
    """
    def test_init(self):
        """Test basic RingBuffer initialization.
        """
        buf = RingBuffer(10)
        self.assertEqual(buf.max_len, 10)

    def test_init_invalid_size_type(self):
        """Test RingBuffer initialization with an invalid size parameter.
        """
        self.assertRaisesRegex(ValueError, 'Invalid maximum buffer size',
                               RingBuffer, 'abc')

    def test_init_invalid_size_negative(self):
        """Test RingBuffer initialization with an invalid size parameter.
        """
        self.assertRaisesRegex(ValueError, 'Invalid maximum buffer size',
                               RingBuffer, -5)

    def test_init_invalid_size_zero(self):
        """Test RingBuffer initialization with an invalid size parameter.
        """
        self.assertRaisesRegex(ValueError, 'Invalid maximum buffer size',
                               RingBuffer, 0)

    def test_enqueue_invalid_chunk_type(self):
        """Test the RingBuffer.enqueue() method with a chunk of invalid type.
        """
        self.assertRaisesRegex(ValueError,
                               '\'chunk\' must implement the buffer protocol',
                               RingBuffer(5).enqueue, None)

    def test_enqueue_chunk_too_big(self):
        """Test basic RingBuffer.enqueue() with a chunk that is larger than
        the available space on the buffer.
        """
        buf = RingBuffer(5)
        self.assertRaisesRegex(ValueError,
                               re.escape('Chunk (6 bytes) is larger than '
                                         'buffer space available (5)'),
                               buf.enqueue, b'123456')

    def test_enqueue(self):
        """Test RingBuffer.enqueue().
        """
        data0, data1 = b'123', b'6789'
        buf = RingBuffer(10)
        buf.enqueue(data0)
        self.assertEqual(buf._dq[0], data0)
        buf.enqueue(data1)
        self.assertEqual(buf._dq[1], data1)
        self.assertEqual(len(buf), len(data0) + len(data1))

    def test_dequeue_invalid_count_negative(self):
        """Test RingBuffer.dequeue() with a count of invalid type.
        """
        self.assertRaisesRegex(ValueError, 'Invalid count',
                               RingBuffer(5).dequeue, -5)

    def test_dequeue_empty(self):
        """Test RingBuffer.dequeue() on an empty buffer.
        """
        buf = RingBuffer(10)
        self.assertEqual(buf.dequeue(5), b'')

    def test_dequeue_tail_chunk(self):
        """Test RingBuffer.dequeue() when data requested is available in the
        tail chunk.
        """
        buf = RingBuffer(10)
        buf.enqueue(b'abcde')
        self.assertEqual(buf.dequeue(2), b'ab')
        self.assertEqual(buf._dq[0], b'cde')

    def test_dequeue_tail_chunk_some_available(self):
        """Test RingBuffer.dequeue() when less data than requested is available
        in the tail chunk.
        """
        buf = RingBuffer(10)
        buf.enqueue(b'abcde')
        self.assertEqual(buf.dequeue(6), b'abcde')
        self.assertEqual(len(buf._dq), 0)

    def test_dequeue_multiple_chunks(self):
        """Test RingBuffer.dequeue() when multiple chunks have to be popped.
        """
        buf = RingBuffer(10)
        buf.enqueue(b'abcd')
        buf.enqueue(b'12345')
        self.assertEqual(buf.dequeue(8), b'abcd1234')
        self.assertEqual(len(buf._dq), 1)
        self.assertEqual(buf._dq[0], b'5')

    def test_dequeue_all(self):
        """Test RingBuffer.dequeue() when all of the data is requested.
        """
        buf = RingBuffer(20)
        buf.enqueue(b'abcd')
        buf.enqueue(b'12345')
        buf.enqueue(b'xyz')
        self.assertEqual(buf.dequeue(), b'abcd12345xyz')
        self.assertEqual(len(buf._dq), 0)

    def test_peek_invalid_count(self):
        """Test RingBuffer.peek() with a count of invalid type.
        """
        self.assertRaisesRegex(ValueError, 'Invalid count',
                               RingBuffer(5).peek, -5)

    def test_peek_empty(self):
        """Test RingBuffer.peek() on an empty buffer.
        """
        buf = RingBuffer(10)
        self.assertEqual(buf.peek(5), b'')

    def test_peek_tail_chunk(self):
        """Test RingBuffer.peek() when data requested is available in the
        tail chunk.
        """
        data0 = b'abcde'
        buf = RingBuffer(10)
        buf.enqueue(b'abcde')
        self.assertEqual(buf.peek(2), b'ab')
        self.assertEqual(buf._dq[0], data0)

    def test_peek_tail_chunk_some_available(self):
        """Test RingBuffer.peek() when less data than requested is available
        in the tail chunk.
        """
        data0 = b'abcde'
        buf = RingBuffer(10)
        buf.enqueue(data0)
        self.assertEqual(buf.peek(6), data0)
        self.assertEqual(buf._dq[0], data0)

    def test_peek_multiple_chunks(self):
        """Test RingBuffer.peek() when multiple chunks have to be popped.
        """
        data0, data1 = b'abcd', b'12345'
        buf = RingBuffer(10)
        buf.enqueue(data0)
        buf.enqueue(data1)
        self.assertEqual(buf.peek(8), b'abcd1234')
        self.assertEqual(len(buf._dq), 2)
        self.assertEqual(buf._dq[0], data0)
        self.assertEqual(buf._dq[1], data1)

    def test_peek_all(self):
        """Test RingBuffer.peek() when all of the data is requested.
        """
        data0, data1, data2 = b'abcd', b'12345', b'xyz'
        buf = RingBuffer(20)
        buf.enqueue(data0)
        buf.enqueue(data1)
        buf.enqueue(data2)
        self.assertEqual(buf.peek(), data0 + data1 + data2)
        self.assertEqual(len(buf._dq), 3)
        self.assertEqual(buf._dq[0], data0)
        self.assertEqual(buf._dq[1], data1)
        self.assertEqual(buf._dq[2], data2)

    def test_find_after_peek(self):
        """Test RingBuffer.find() after a peek operation.
        """
        buf = RingBuffer(20)
        buf.enqueue(b'123')
        buf.enqueue(b'xyz')
        self.assertEqual(buf.peek(4), b'123x')
        self.assertEqual(buf.find(b'z'), 5)

    def test_find(self):
        """Test RingBuffer.find().
        """
        buf = RingBuffer(20)
        buf.enqueue(b'123')
        self.assertEqual(buf.find(b'2'), 1)
        self.assertEqual(len(buf._dq), 1)

    def test_find_empty(self):
        """Test RingBuffer.find().
        """
        buf = RingBuffer(20)
        self.assertEqual(buf.find(b'2'), -1)

    def test_find_after_read(self):
        """Test RingBuffer.find() after a dequeue operation.
        """
        buf = RingBuffer(20)
        buf.enqueue(b'123')
        buf.enqueue(b'xyz')
        self.assertEqual(buf.dequeue(4), b'123x')
        self.assertEqual(buf.find(b'z'), 1)

    def test_find_multiple_chunks(self):
        """Test RingBuffer.find() when multiple chunks are involved.
        """
        buf = RingBuffer(20)
        buf.enqueue(b'123')
        buf.enqueue(b'xyz')
        self.assertEqual(buf.find(b'xy'), 3)
        self.assertEqual(len(buf._dq), 1)
        self.assertEqual(buf._dq[0], b'123xyz')
        self.assertEqual(len(buf._dq), 1)

    def test_find_invalid_delimiter(self):
        """Test RingBuffer.find() with an invalid delimiter.
        """
        self.assertRaisesRegex(ValueError,
                               '\'delimiter\' must provide the buffer '
                               'interface',
                               RingBuffer(20).find, 123)


class ChannelConfTestMixin(object):
    """Helpers for testing the ChannelConf subclasses.
    """
    def mock_transport_conf(self, blocking=False):
        """Create and return a mock ITransportConf instance.
        """
        transport_conf = mock.MagicMock()
        directlyProvides(transport_conf, interfaces.ITransportConf)
        transport_conf.blocking = blocking
        return transport_conf

    def mock_message_envelope(self):
        """Create and return a mock IMessageEnvelope instance.
        """
        message_envelope = mock.MagicMock()
        directlyProvides(message_envelope, interfaces.IMessageEnvelope)
        return message_envelope


class ChannelConfTest(unittest.TestCase, ChannelConfTestMixin):
    """Test the ChannelConf class.
    """
    def test_init(self):
        """Initialize a TornadoChannelConf instance.
        """
        transport_conf = self.mock_transport_conf()
        message_envelope = self.mock_message_envelope()
        conf = ChannelConf(transport_conf, message_envelope, receive_timeout=20)
        verifyObject(interfaces.IChannelConf, conf)
        self.assertEqual(conf.transport_conf, transport_conf)
        self.assertEqual(conf.message_envelope, message_envelope)
        self.assertEqual(conf.receive_timeout, 20)

    def test_init_invalid_transport_conf(self):
        """Initialize a TornadoChannelConf instance with a transport argument
        that does not provide the ITransportConf interface.
        """
        self.assertRaisesRegex(ValueError,
                               'Transport configuration object must provide '
                               'ITransportConf',
                               ChannelConf,
                               mock.MagicMock(), mock.MagicMock(), 12)

    def test_init_invalid_message_envelope(self):
        """Initialize a TornadoChannelConf instance with a message_envelope
        argument that does not provide the IMessageEnvelope interface.
        """
        self.assertRaisesRegex(ValueError,
                               'Message envelope object must provide '
                               'IMessageEnvelope',
                               ChannelConf,
                               self.mock_transport_conf(), mock.MagicMock(), 12)

    def test_init_invalid_receive_timeout(self):
        """Initialize a TornadoChannelConf instance with an invalid
        receive_timeout argument
        """
        self.assertRaisesRegex(ValueError,
                               'Invalid receive timeout value',
                               ChannelConf,
                               self.mock_transport_conf(),
                               self.mock_message_envelope(), -2)


if __name__ == '__main__':
    unittest.main()
