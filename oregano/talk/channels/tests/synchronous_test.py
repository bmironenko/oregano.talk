"""SynchronousChannel (and related) tests.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

from zope.interface import directlyProvides
from zope.interface.verify import verifyObject
from zope.component import getUtility
from .common_test import ChannelConfTestMixin
from ..synchronous import SynchronousChannel, SynchronousChannelConf, \
    _ISynchronousChannelConf
from ....talk import interfaces, exceptions_
from ....test import *


# Full name of the module being tested - to avoid hard-coding mock targets.
target_module_name = '.'.join(__name__.split('.')[:-2] + ['synchronous'])


def _target(name):
    """Return the full name (including packages) of a target to patch,
    assuming that it is imported from the code module that corresponds to this
    test module.
    """
    return target_module_name + '.' + name


class SynchronousChannelConfTest(unittest.TestCase, ChannelConfTestMixin):
    """Test the TornadoChannelConf class.
    """
    def test_init_non_blocking_transport(self):
        """Initialize a SynchronousChannelConf with a transport that is
        non-blocking.
        """
        transport_conf = self.mock_transport_conf()
        self.assertRaisesRegex(ValueError,
                               'Must use a blocking transport',
                               SynchronousChannelConf,
                               transport_conf, self.mock_message_envelope(), 15)

    def test_init_read_buffer_size_None(self):
        """Initialize a SynchronousChannelConf with an invalid read buffer size.
        """
        self.assertRaisesRegex(ValueError,
                               'Invalid read buffer size',
                               SynchronousChannelConf,
                               self.mock_transport_conf(True),
                               self.mock_message_envelope(), 15, None)

    def test_init_read_buffer_size_negative(self):
        """Initialize a SynchronousChannelConf with an invalid read buffer size.
        """
        self.assertRaisesRegex(ValueError,
                               'Invalid read buffer size',
                               SynchronousChannelConf,
                               self.mock_transport_conf(True),
                               self.mock_message_envelope(), 15, -5)

    def test_init_read_buffer_size_zero(self):
        """Initialize a SynchronousChannelConf with an invalid read buffer size.
        """
        self.assertRaisesRegex(ValueError,
                               'Invalid read buffer size',
                               SynchronousChannelConf,
                               self.mock_transport_conf(True),
                               self.mock_message_envelope(), 15, 0)

    def test_init_factory(self):
        """Initialize a SynchronousChannelConf instance using a factory.
        """
        factory = getUtility(interfaces.IChannelConfFactory, 'synchronous')
        conf = factory(self.mock_transport_conf(True),
                       self.mock_message_envelope(),
                       12)
        verifyObject(_ISynchronousChannelConf, conf)

    def test_init_factory_invalid_args(self):
        """Initialize a SynchronousChannelConf instance using a factory with
        an invalid argument set.
        """
        self.assertRaises(TypeError,
                          getUtility(interfaces.IChannelConfFactory,
                                     'synchronous'))


class SynchronousChannelTest(unittest.TestCase):
    """Test the TornadoChannel class.
    """
    def mock_conf(self, receive_timeout=2, receive_buffer_size=2048):
        """Create a mock IChannelConf instance.
        """
        conf = mock.MagicMock(spec=SynchronousChannelConf)
        directlyProvides(conf, interfaces.IChannelConf)
        directlyProvides(conf.transport_conf, interfaces.ITransportConf)
        directlyProvides(conf.message_envelope, interfaces.IMessageEnvelope)
        conf.receive_timeout = receive_timeout
        conf.receive_buffer_size = receive_buffer_size
        return conf

    def create_channel(self, receive_timeout=2,
                       receive_buffer_size=2048, receive_buffer=None):
        """Create a mock SynchronousChannel instance.
        """
        conf = self.mock_conf(receive_timeout=receive_timeout,
                              receive_buffer_size=receive_buffer_size)
        with mock.patch(_target('ITransport')):
            channel = SynchronousChannel(conf)
        if receive_buffer:
            channel._receive_buffer.enqueue(receive_buffer)
        return channel

    @mock.patch(_target('ITransport'))
    def test_init(self, ITransport):
        """Test basic SynchronousChannel initialization.
        """
        conf = self.mock_conf()
        channel = SynchronousChannel(conf)
        verifyObject(interfaces.IChannel, channel)
        ITransport.assert_called_once_with(conf.transport_conf)
        self.assertEqual(channel.conf, conf)
        self.assertEqual(channel._transport, ITransport.return_value)

    def test_init_invalid_conf(self):
        """Test basic SynchronousChannel initialization with an invalid
        configuration object.
        """
        self.assertRaisesRegex(ValueError,
                               'Configuration object must be an instance of '
                               'SynchronousChannelConf',
                               SynchronousChannel, mock.MagicMock())

    @mock.patch(_target('ITransport'))
    def test_init_with_connection_transport(self, ITransport):
        """Initialize a SynchronousChannel instance with a transport that
        requires connecting.
        """
        transport = ITransport.return_value
        directlyProvides(transport, interfaces.IConnection)
        transport.connected = False
        channel = SynchronousChannel(self.mock_conf())
        channel._transport.connect.assert_called_once_with()

    def test_read_into_buffer(self):
        """Test the SynchronousChannel._read_into_buffer() method.
        """
        channel = self.create_channel(receive_buffer=b'123')
        channel._transport.read.return_value = b'12'
        self.assertEqual(channel._read_into_buffer(count=2), 2)
        channel._transport.read.assert_called_once_with(count=2)
        self.assertEqual(channel._receive_buffer.peek(), b'12312')

    def test_read_into_buffer_count_None(self):
        """Test the SynchronousChannel._read_into_buffer() method with a count
        parameter that is None.
        """
        channel = self.create_channel(receive_buffer=b'123')
        channel._transport.read.return_value = b'123'
        self.assertEqual(channel._read_into_buffer(), 3)
        channel._transport.read.assert_called_once_with(count=None)
        self.assertEqual(channel._receive_buffer.peek(), b'123123')

    def test_read_into_buffer_no_data(self):
        """Test the SynchronousChannel._read_into_buffer() method when the
        underlying read does not return any data.
        """
        channel = self.create_channel(receive_buffer=b'123')
        channel._transport.read.return_value = None
        self.assertEqual(channel._read_into_buffer(), 0)
        channel._transport.read.assert_called_once_with(count=None)
        self.assertEqual(channel._receive_buffer.peek(), b'123')

    def test_read(self):
        """Test the SynchronousChannel._read() method when reading all
        available data.
        """
        channel = self.create_channel(receive_buffer=b'123')
        channel._transport.read.return_value = b'456'
        self.assertEqual(channel._read(), b'123456')
        self.assertEqual(channel._receive_buffer.peek(), b'')
        channel._transport.read.assert_called_once_with(count=None)

    def test_read_count_buffered(self):
        """Test the SynchronousChannel._read() method when a count is
        specified and all of the data is available in the buffer.
        """
        channel = self.create_channel(receive_buffer=b'1234')
        channel._transport.read.return_value = b'567'
        self.assertEqual(channel._read(count=4), b'1234')
        self.assertEqual(channel._receive_buffer.peek(), b'567')
        channel._transport.read.assert_called_once_with(count=None)

    def test_read_count_buffered_and_more(self):
        """Test the SynchronousChannel._read() method when a count is
        specified and some of the data must be read directly from the transport.
        """
        channel = self.create_channel()
        channel._transport.read.side_effect = [b'1234', b'5']
        self.assertEqual(channel._read(count=5), b'12345')
        self.assertEqual(channel._receive_buffer.peek(), b'')
        self.assertEqual(channel._transport.read.mock_calls, [
            mock.call(count=None),
            mock.call(count=1)
        ])

    @mock.patch('time.sleep')
    def test_read_until(self, *args):
        """Test the SynchronousChannel._read_until() method.
        """
        channel = self.create_channel()
        channel._transport.read.side_effect = [b'x', b'', b'SE', b'SENy', b'xx']
        self.assertEqual(channel._read_until(b'SEN'), b'xSESEN')
        self.assertEqual(channel._receive_buffer.peek(), b'y')

    @mock.patch('time.sleep')
    def test_read_until_timeout(self, sleep):
        """Test SynchronousChannel._read_until() with a simulated timeout.
        """
        channel = self.create_channel()
        channel.conf.receive_timeout = 5
        channel._READ_SLEEP_INTERVAL = 1
        channel._transport.read.return_value = b''
        self.assertRaisesRegex(exceptions_.ChannelTimeoutError,
                               'Timed out while waiting for '
                               '{delim!r}'.format(delim=b'SEN'),
                               channel._read_until, b'SEN')
        self.assertEqual(len(sleep.mock_calls), 5)

    @mock.patch('time.sleep')
    def test_read_until_no_timeout(self, sleep):
        """Test SynchronousChannel._read_until() when no timeout is configured.
        """
        channel = self.create_channel(receive_timeout=None)
        channel._READ_SLEEP_INTERVAL = 1
        channel._transport.read.side_effect = [b'', b'', b'', b'', b'SEN']
        channel._read_until(b'SEN')
        self.assertEqual(len(sleep.mock_calls), 4)

    @mock.patch('time.sleep')
    def test_read_until_buffered(self, sleep):
        """Test SynchronousChannel._read_until() when all the data needed
        has already been buffered
        """
        channel = self.create_channel(receive_buffer=b'garbageABCdef')
        channel.conf.receive_timeout = 1
        channel._READ_SLEEP_INTERVAL = 1
        channel._transport.read.side_effect = [b'']
        self.assertEqual(channel._read_until(b'ABC'), b'garbageABC')

    def test_send(self):
        """Test basic SynchronousChannel.send() usage.
        """
        data = b'ABCDE'
        channel = self.create_channel()
        self.assertTrue(channel.send(data))
        channel.conf.message_envelope.wrap.assert_called_once_with(data)
        channel._transport.write.assert_called_once_with(
            channel.conf.message_envelope.wrap.return_value)

    @mock.patch('time.sleep')
    def test_receive(self, *args):
        """Test basic SynchronousChannel.receive() usage.
        """
        sentinel, data = b'SEN', b'ABCDE'
        channel = self.create_channel()
        # Transport reads will first return the length and then the data.
        channel._transport.read.side_effect = [sentinel, data]
        unwrap = mock_generator((True, sentinel), (True, 5), (False, data[1:]))
        channel.conf.message_envelope.unwrap.return_value = unwrap
        # Subsequent envelope instructions will ask for 5 bytes and then
        # return the subset of the data.
        # unwrap.send.side_effect = []
        message = channel.receive()
        self.assertEqual(message, data[1:])
        if sys.version_info[0] >= 3:
            unwrap.__next__.assert_called_once_with()
            self.assertEqual(unwrap.send.mock_calls, [
                mock.call(sentinel),
                mock.call(data)
            ])

    @mock.patch('time.sleep')
    def test_receive_timeout(self, *args):
        """Test SynchronousChannel.receive() with a simulated timeout.
        """
        sentinel, data = b'SEN', b'ABCDE'
        channel = self.create_channel()
        channel._transport.read.return_value = b''
        unwrap = mock_generator((True, sentinel))
        channel.conf.message_envelope.unwrap.return_value = unwrap
        self.assertRaisesRegex(exceptions_.ChannelTimeoutError,
                               'Timed out while waiting for '
                               '{delim!r}'.format(delim=b'SEN'),
                               channel.receive)

if __name__ == '__main__':
    unittest.main()

