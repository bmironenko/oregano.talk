"""TornadoChannel (and related) tests.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

from concurrent.futures import Future
import os
from struct import pack
from tornado.iostream import PipeIOStream
from tornado.testing import AsyncTestCase, gen_test
from zope.interface import directlyProvides
from zope.interface.verify import verifyObject
from zope.component import getUtility
from .common_test import ChannelConfTestMixin
from ..tornado import *
from ...envelopes.header import MessageHeader
from ....test import *
from ....talk import interfaces


# Full name of the module being tested - to avoid hard-coding mock targets.
target_module_name = '.'.join(__name__.split('.')[:-2] + ['tornado'])


def _target(name):
    """Return the full name (including packages) of a target to patch,
    assuming that it is imported from the code module that corresponds to this
    test module.
    """
    return target_module_name + '.' + name


class TornadoChannelConfTest(unittest.TestCase, ChannelConfTestMixin):
    """Test the TornadoChannelConf class.
    """
    def test_init_blocking_transport(self):
        """Initialize a TOrnadoChannelConf with a transport that is blocking.
        """
        transport_conf = self.mock_transport_conf()
        transport_conf.blocking = True
        self.assertRaisesRegex(ValueError,
                               'Must use a non-blocking transport',
                               TornadoChannelConf,
                               transport_conf, self.mock_message_envelope(), 15)

    def test_init_factory(self):
        """Initialize a TornadoChannelConf instance using a factory.
        """
        factory = getUtility(interfaces.IChannelConfFactory, 'tornado')
        conf = factory(self.mock_transport_conf(),
                       self.mock_message_envelope(),
                       12)
        verifyObject(interfaces.IChannelConf, conf)

    def test_init_factory_invalid_args(self):
        """Initialize a TornadoChannelConf instance using a factory with
        an invalid argument set.
        """
        self.assertRaises(TypeError,
                          getUtility(interfaces.IChannelConfFactory, 'tornado'))


class TransportStreamTest(unittest.TestCase):
    """Tests for the private _IOStream class.
    """
    def setUp(self):
        """Set up a "standard" TransportStream instance suitable for most tests.
        This also verifies basic initialization.
        """
        transport = self.mock_transport()
        self.create_stream(transport)

    def mock_transport(self, connected=True, blocking=False):
        """Create and configure a mock ITransport instance. Can be used in
        place of mock.patch() when patching ITransport.
        """
        transport = mock.MagicMock()

        def _connect():
            transport.connected = True
            transport.fileno = 10

        # Mock implement appropriate interfaces.
        ifaces = [interfaces.ITransport, interfaces.IFileDescriptor]
        if connected is not None:
            ifaces.append(interfaces.IConnection)
            transport.connect = mock.MagicMock(side_effect=_connect)
            transport.connected = connected
        directlyProvides(transport, ifaces)
        transport.conf.blocking = blocking

        return transport

    def create_stream(self, transport=None, *args, **kwargs):
        """Create a TransportStream instance.
        """
        if not transport:
            transport = self.mock_transport()
        self.stream = TransportStream(transport, *args, **kwargs)
        self.assertEqual(self.stream._transport, transport)

    def test_init(self):
        """Test the basic TransportStream.__init__() usage.
        """
        with mock.patch('tornado.iostream.BaseIOStream.__init__') as init:
            kwargs = dict(io_loop='loop', max_buffer_size=123,
                          read_chunk_size=50)
            self.stream = TransportStream(self.mock_transport(), **kwargs)
            init.assert_called_once_with(**kwargs)

    def test_init_with_blocking_transport(self):
        """Test TransportStream.__init__() with a blocking transport.
        """
        self.assertRaisesRegex(ValueError,
                               'Cannot create a TransportIOStream with a '
                               'blocking ITransport', TransportStream,
                               self.mock_transport(blocking=True))

    def test_init_with_invalid_transport(self):
        """Test TransportStream.__init__() with an invalid transport.
        """
        self.assertRaisesRegex(ValueError,
                               "Transport object must provide ITransport",
                               TransportStream,
                               'invalid_transport')

    def test_init_with_non_fd_transport(self):
        """Test TransportStream.__init__() with an invalid transport.
        """
        transport = mock.MagicMock()
        directlyProvides(transport, interfaces.ITransport)
        self.assertRaisesRegex(ValueError,
                               "Transport object must provide IFileDescriptor",
                               TransportStream,
                               transport)

    def test_init_with_plain_transport(self):
        """Test TransportStream.__init__() with a non-IConnection transport.
        """
        self.create_stream(self.mock_transport(connected=None))
        self.assertEqual(len(self.stream._transport.connect.mock_calls), 0)

    def test_init_with_connection_transport(self):
        """Initialize a TransportStream instance with a transport that requires
        connecting.
        """
        self.create_stream(self.mock_transport(connected=False))
        self.stream._transport.connect.assert_called_once_with()

    def test_init_with_connected_transport(self):
        """Initialize a TransportStream instance with a transport that requires
        connecting and is already connected.
        """
        self.create_stream(self.mock_transport(connected=True))
        self.assertEqual(len(self.stream._transport.connect.mock_calls), 0)

    def test_fileno(self):
        """Test the TransportStream.fileno() method with an unconnected
        underlying transport (should autoconnect).
        """
        self.create_stream(self.mock_transport(connected=False))
        self.assertEqual(self.stream.fileno(), 10)

    def test_close(self):
        """Test the TransportStream.close() method.
        """
        self.stream.close_fd()
        self.stream._transport.close.assert_called_once_with()

    def test_close_plain_transport(self):
        """Test the TransportStream.close() method with a
        non-IConnection transport.
        """
        self.create_stream(self.mock_transport(connected=None))
        self.stream.close_fd()
        self.assertEqual(len(self.stream._transport.close.mock_calls), 0)

    def test_close_already_closed(self):
        """Test the TransportStream.close() method with an already
        closed transport.
        """
        self.create_stream(self.mock_transport(connected=False))
        self.stream.close_fd()
        self.assertEqual(len(self.stream._transport.close.mock_calls), 1)

    def test_write_to_fd(self):
        """Test the TransportStream.write_to_fd() method with valid input.
        """
        self.stream._transport.write.return_value = 3
        self.assertEqual(self.stream.write_to_fd(b'abc'), 3)
        self.stream._transport.write.assert_called_once_with(b'abc')

    def test_read_from_fd(self):
        """Test the TransportStream.read_from_fd() method.
        """
        self.stream._transport.read.return_value = b'abcdefg'
        self.assertEqual(self.stream.read_from_fd(), b'abcdefg')
        self.stream._transport.read.assert_called_once_with(
            count=self.stream.read_chunk_size)

    def test_read_from_fd_nothing_to_read(self):
        """Test the TransportStream.write_to_fd() method when there is nothing
        to read.
        """
        self.create_stream(read_chunk_size=10)
        self.stream._transport.read.return_value = None
        self.assertEqual(self.stream.read_from_fd(), None)
        self.stream._transport.read.assert_called_once_with(count=10)


class StreamChannelTest(AsyncTestCase):
    """Test the StreamChannel class.
    """
    def setUp(self):
        """Set up a "standard" StreamChannel instance suitable for most tests.
        This also verifies basic initialization.
        """
        super(StreamChannelTest, self).setUp()
        # Use a pipe for testing - easiest thing to do.
        self.read_fd, self.write_fd = os.pipe()

    def create_stream(self, fd):
        """Create and configure a new IOStream instance.
        :param fd:
            File descriptor to use for the stream.
        """
        return PipeIOStream(fd)

    def test_init_bad_stream(self):
        """Test StreamChannel.__init__() with a stream argument that does not
        inherit from tornado.iostream.BaseIOStream.
        """
        self.assertRaises(ValueError,
                          StreamChannel,
                          mock.MagicMock,
                          MessageHeader())

    def test_init_bad_envelope(self):
        """Test StreamChannel.__init__() with an envelope argument that does not
        provide IMessageEnvelope interface.
        """
        self.assertRaises(ValueError, StreamChannel,
                          self.create_stream(self.read_fd),
                          mock.MagicMock)

    @gen_test
    def test_send_none(self):
        """Test StreamChannel.send() with data that evaluates to None.
        """
        channel = StreamChannel(self.create_stream(self.write_fd),
                                MessageHeader())
        self.assertRaisesRegex(ValueError,
                               '\'message\' must implement the buffer protocol',
                               channel.send, None)

    @gen_test
    def test_send(self):
        """Test StreamChannel.send() end-to-end with an actual underlying
        stream.
        """
        channel = StreamChannel(
            self.create_stream(self.write_fd),
            MessageHeader())

        # Send data over the pipe.
        data = b'ABC'
        expected = bytearray('\x02\x03', encoding='UTF-8')
        expected.extend(pack('<H', len(data)))
        expected.extend(pack('<3s', data))
        result = channel.send(data)
        self.assertIsInstance(result, Future)
        sent = yield result
        self.assertTrue(sent)

        # Read from the pipe and verify the data.
        actual = os.read(self.read_fd, len(expected))
        self.assertEqual(actual, expected)

    @gen_test
    def test_receive(self):
        """Test StreamChannel.receive().
        """
        channel = StreamChannel(
            self.create_stream(self.read_fd),
            MessageHeader())

        # Send data over the pipe.
        data = b'DEFG'
        to_send = bytearray('XXXX\x02\x03', encoding='UTF-8')
        to_send.extend(pack('<H', len(data)))
        to_send.extend(pack('<4s', data))
        os.write(self.write_fd, bytes(to_send))

        # Receive from the channel.
        result = channel.receive()
        self.assertIsInstance(result, Future)
        actual = yield result
        self.assertEqual(actual, data)

    @gen_test
    def test_receive_error(self):
        """Test StreamChannel.receive() when the asynchronous operation raises
        an exception.
        """
        stream = self.create_stream(self.read_fd)
        stream.read_bytes = mock.MagicMock(side_effect=IOError)
        channel = StreamChannel(stream, MessageHeader())

        # Send data over the pipe.
        os.write(self.write_fd, b'\x02\x03')

        # Receive from the channel.
        future = channel.receive()
        self.assertIsInstance(future, Future)
        try:
            yield future
        except IOError as ex:
            print(ex)
        else:
            self.fail("IOError was expected to be raised")
        self.assertTrue(future.done())
        self.assertIsInstance(future.exception(), IOError)

    # TODO: configure and test receive timeouts (and/or incomplete messages)


class TornadoChannelTest(unittest.TestCase):
    """Test the TornadoChannel class.
    """
    def mock_conf(self):
        """Create a mock IChannelConf instance.
        """
        conf = mock.MagicMock(spec=TornadoChannelConf)
        directlyProvides(conf, interfaces.IChannelConf)
        directlyProvides(conf.transport_conf, interfaces.ITransportConf)
        return conf

    @mock.patch(_target('StreamChannel.__init__'))
    @mock.patch(_target('TransportStream'))
    @mock.patch(_target('ITransport'))
    def test_init(self, ITransport, TransportStream, stream_channel_init):
        """Test basic TornadoChannel initialization.
        """
        conf = self.mock_conf()
        channel = TornadoChannel(conf)
        verifyObject(interfaces.IChannel, channel)
        ITransport.assert_called_once_with(conf.transport_conf)
        TransportStream.assert_called_once_with(ITransport.return_value)
        stream_channel_init.assert_called_once_with(
            TransportStream.return_value,
            conf.message_envelope)
        self.assertEqual(channel.conf, conf)

    def test_init_invalid_conf(self):
        """Test basic TornadoChannel initialization with an invalid
        configuration object.
        """
        self.assertRaisesRegex(ValueError,
                               'Configuration object must be an instance of '
                               'TornadoChannelConf',
                               TornadoChannel, mock.MagicMock())

if __name__ == '__main__':
    unittest.main()
