"""Asynchronous channel implementation for the Tornado framework.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'TransportStream',
    'StreamChannel',
    'TornadoChannelConf',
    'TornadoChannel',
]

from concurrent.futures import Future
from functools import partial
import logging
from tornado.iostream import BaseIOStream
from zope.interface import implementer
from zope.component import adapter
from zope.component.factory import Factory
from zope.component import getGlobalSiteManager
from .common import ChannelConf
from ..interfaces import *


class TransportStream(BaseIOStream):
    """Implements a Tornado IO stream based on the ITransport interface.
    """
    _logger = logging.getLogger(__name__ + '.TransportStream')

    def __init__(self, transport, *args, **kwargs):
        """Initialize a new TransportIOStream instance.

        :param oregano.talk.interfaces.ITransport transport: transport to use.
        :exception ValueError: `transport` does not provide \
            :class:`oregano.talk.interfaces.ITransport` or \
            :class:`oregano.talk.interfaces.IFileDescriptor`, \
            or is configured in blocking mode.
        """
        self._logger.debug('Creating with: transport=%r', transport)

        error = None
        if not ITransport.providedBy(transport):
            error = 'Transport object must provide ITransport'
        elif not IFileDescriptor.providedBy(transport):
            error = 'Transport object must provide IFileDescriptor'
        elif transport.conf.blocking:
            error = 'Cannot create a TransportIOStream with a ' \
                    'blocking ITransport'
        if error:
            self._logger.error(error)
            raise ValueError(error)

        self._transport = transport
        self._is_connection_transport = IConnection.providedBy(self._transport)
        if self._is_connection_transport and not self._transport.connected:
            self._transport.connect()
        super(TransportStream, self).__init__(*args, **kwargs)

    def fileno(self):
        """Return the file descriptor for this stream.

        :return: stream file descriptor.
        """
        return self._transport.fileno

    def close_fd(self):
        """Close the file underlying this stream.
        """
        if self._is_connection_transport and self._transport.connected:
            self._logger.debug('Closing fd %r', self.fileno())
            self._transport.close()

    def write_to_fd(self, data):
        """Attempt to write data to the underlying file.

        :param bytes data: data to write.
        :return: number of bytes written.
        """
        self._logger.debug('Writing to fd %r', self.fileno())
        return self._transport.write(data)

    def read_from_fd(self):
        """Attempt to read from the underlying file.

        :return: :const:`None` if there was nothing to read (the socket \
            returned :const:`EWOULDBLOCK` or equivalent), otherwise returns \
            the data. When possible, should return no more than \
            :literal:`self.read_chunk_size` bytes at a time.
        """
        self._logger.debug('Reading from fd %r', self.fileno())
        bytes_read = self._transport.read(count=self.read_chunk_size)
        return bytes_read


class StreamChannel(object):
    """Implements a channel based on a Tornado IO stream.
    """
    _logger = logging.getLogger(__name__ + '.StreamChannel')

    def __init__(self, stream, envelope):
        """Initialize a new BaseTornadoChannel instance.

        :param tornado.iostream.BaseIOStream stream: stream instance.
        :param oregano.talk.interfaces.IMessageEnvelope envelope: message \
            envelope to use.
        :exception ValueError: `stream` is not an instance of \
            :class:`tornado.iostream.BaseIOStream`, or `envelope` does not \
            provide :class:`oregano.talk.interfaces.IMessageEnvelope`.
        """
        self._logger.debug('Creating with: stream=%r', stream)

        error = None
        if not isinstance(stream, BaseIOStream):
            error = 'Stream must be an instance of BaseIOStream or a subclass'
        elif not IMessageEnvelope.providedBy(envelope):
            error = 'Envelope object must provide IMessageEnvelope'
        if error:
            self._logger.error(error)
            raise ValueError(error)

        self._stream = stream
        self._envelope = envelope

    def send(self, message):
        """Send a message, wrapping it in an envelope/header.

        :param bytes message: message to send.
        :return: a :class:`concurrent.futures.Future` object.
        :exception ValueError: `message` does not provide the buffer interface.
        """
        if not isinstance(message, (bytes, bytearray, memoryview)):
            error = '\'message\' must implement the buffer protocol'
            self._logger.error(error)
            raise ValueError(error)

        self._logger.debug('Asynchronously sending a message of length %r',
                           len(message))
        future = Future()
        self._stream.write(self._envelope.wrap(message),
                           lambda: future.set_result(True))
        future.set_running_or_notify_cancel()
        return future

    def receive(self):
        """Asynchronously receive the next message.

        :return: a :class:`concurrent.futures.Future` object that, when \
            yielded, will return the received message.
        """
        self._logger.debug('Asynchronously receiving a message')
        future = Future()
        future.set_running_or_notify_cancel()
        gen = self._envelope.unwrap()
        self._unwrap(future, gen)
        return future

    def _unwrap(self, future, gen, data=None):
        """Process the next unwrap instructions.

        :param concurrent.futures.Future future: a future object associated \
            with the current asynchronous receive operation.
        :param generator gen: current unwrap generator.
        :param bytes data: channel data received, or None if this is the start \
            of the unwrap process.
        """
        try:
            more, bytes_ = next(gen) if data is None else gen.send(data)
            if not more:
                future.set_result(bytes_)
            else:
                callback = partial(self._unwrap, future, gen)
                if isinstance(bytes_, bytes):
                    self._stream.read_until(bytes_, callback)
                else:
                    self._stream.read_bytes(bytes_, callback)
        except Exception as ex:
            # Tornado can handle exceptions raised from coroutines, but
            # set_exception() will set the future state to FINISHED, so it's
            # a good thing to do.
            future.set_exception(ex)
            raise


class _ITornadoChannelConf(IChannelConf):  # pragma: no cover
    """Marker interface for the :class:`SerialTransportConf` class; exists for
    adapter registration purposes.
    """


@implementer(_ITornadoChannelConf)
class TornadoChannelConf(ChannelConf):
    """Configuration for the tornado message channel.
    """
    def __init__(self, transport_conf, message_envelope, receive_timeout=None):
        """Initialize a new TornadoChannelConf instance.

        :param oregano.talk.interfaces.ITransportConf transport_conf:
            transport configuration.
        :param oregano.talk.interfaces.IMessageEnvelope message_envelope:
            message envelope.
        :param receive_timeout: timeout for receiving messages, in seconds.
        :exception ValueError: `transport_conf` does not provide \
            :class:`oregano.talk.interfaces.ITransportConf`,
            `message_envelope` does not provide \
            :class:`oregano.talk.interfaces.IMessageEnvelope`, or an invalid \
            `receive_timeout` is specified.
        """
        super(TornadoChannelConf, self).__init__(transport_conf,
                                                 message_envelope,
                                                 receive_timeout)
        if transport_conf.blocking:
            error = 'Must use a non-blocking transport'
            self._logger.error(error)
            raise ValueError(error)


@implementer(IChannel)
@adapter(_ITornadoChannelConf)
class TornadoChannel(StreamChannel):
    """Implements a channel based on a TransportStream.
    """
    _logger = logging.getLogger(__name__ + '.TornadoChannel')

    def __init__(self, conf):
        """Initialize a new TornadoChannel instance.

        :param oregano.talk.channels.tornado.TornadoChannelConf conf: \
            channel configuration object.
        :exception ValueError: `conf` is not an instance of \
            :class:`TornadoChannelConf`.
        """
        self._logger.debug('Creating with: conf=%r', conf)
        if not isinstance(conf, TornadoChannelConf):
            error = 'Configuration object must be an instance of ' \
                    'TornadoChannelConf'
            self._logger.error(error)
            raise ValueError(error)
        self._conf = conf
        super(TornadoChannel, self).__init__(
            TransportStream(ITransport(conf.transport_conf)),
            conf.message_envelope)

    @property
    def conf(self):
        """Return the channel configuration object.
        """
        return self._conf


gsm = getGlobalSiteManager()
gsm.registerUtility(Factory(TornadoChannelConf, 'TornadoChannelConf'),
                    IChannelConfFactory, 'tornado')
gsm.registerAdapter(TornadoChannel, (_ITornadoChannelConf,), IChannel, '')
