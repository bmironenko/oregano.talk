"""Package for message envelopes.
"""

from __future__ import absolute_import

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

from . import header as _header
from . import ant as _ant
