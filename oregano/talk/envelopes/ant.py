"""ANT message envelope.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'ANTEnvelope'
]

from functools import reduce
import logging
from struct import pack, unpack, calcsize
from zope.interface import implementer
from zope.component import getGlobalSiteManager
from zope.component.factory import Factory
from ..interfaces import *
from ..exceptions_ import *


@implementer(IMessageEnvelope)
class ANTEnvelope(object):
    """ANT message envelope:

    - SYNC (1 bytes)
    - message length (1 byte)
    - message id (1 byte)
    - message content (N bytes)
    - checksum (1 byte; XOR of all previous bytes including the SYNC byte)
    """
    _SYNC = b'\xA4'
    _FIELD_FMT = '<B'

    _logger = logging.getLogger(__name__ + '.ANTEnvelope')

    def __init__(self):
        """Initialize a new ANTEnvelope instance.
        """
        self._sync = self._SYNC
        self._field_fmt = self._FIELD_FMT
        self._field_num_bytes = calcsize(self._field_fmt)

    def _checksum(self, data):
        """Calculate the checksum for a given sequence of bytes. ANT checksum
        is simply an XOR of all bytes in the sequence.

        :param bytes data: input data.
        :return: checksum byte.
        """
        checksum = 0
        for b in (data or []):
            checksum ^= (b if isinstance(b, int) else ord(b))  # 2.x shim
        return checksum

    def wrap(self, message):
        """Wrap a message in an envelope.

        :param bytes message: message bytes to wrap; first byte is assumed to \
            be the message ID.
        :return: complete wrapped message, ready to be sent on a channel.
        """
        if not isinstance(message, (bytes, bytearray, memoryview)):
            raise ValueError('\'message\' must implement the buffer protocol')
        if len(message) < 1:
            raise ValueError(
                'ANT message must include a message ID byte')
        wrapped_message = bytearray()
        wrapped_message.extend(self._sync)
        wrapped_message.extend(pack(self._field_fmt, len(message) - 1))
        wrapped_message.extend(message)
        wrapped_message.append(self._checksum(wrapped_message))
        return bytes(wrapped_message)

    def unwrap(self):
        """Receive a message. This is a generator yields instructions to the
        client. Each instruction is a *(more, what)* tuple, and can be
        one of:

        - (:const:`True`, :py:class:`int`): caller should read this many bytes;
        - (:const:`True`, :py:class:`bytes`): caller should read until the \
          specified byte sequence;
        - (:const:`False`, :py:class:`bytes`): done; complete message is \
          :py:class:`bytes`; next call to the generator will result in a \
          :py:class:`StopIteration`.

        Caller should send the sequence of bytes read back to the generator.
        """
        complete_message = bytearray()

        # Process the SYNC byte.
        sync = yield True, self._sync
        self._verify_sync(sync)
        complete_message.extend(sync)

        # Process the message content length field.
        content_length_bytes = yield True, self._field_num_bytes
        content_length = self._verify_content_length(content_length_bytes)
        complete_message.extend(content_length_bytes)

        # Process the message ID field.
        message_id_bytes = yield True, self._field_num_bytes
        message_id = self._verify_message_id(message_id_bytes)
        complete_message.extend(message_id_bytes)

        # Process message content.
        content = yield True, content_length
        self._verify_content(content, content_length)
        complete_message.extend(content)

        # Process the checksum byte.
        checksum_bytes = yield True, self._field_num_bytes
        self._verify_checksum(checksum_bytes, complete_message)

        # Done!
        yield False, complete_message[(self._field_num_bytes * 2):]

    def _verify_sync(self, sync_bytes):
        """Verify the SYNC byte.

        :param bytes sync_bytes: SYNC byte (a sequence with, hopefully, \
            one item).
        """
        if not sync_bytes or \
                not hasattr(sync_bytes, '__getitem__') or \
                sync_bytes[-len(self._sync):] != self._sync:
            raise MessageEnvelopeError(
                'Wrong SYNC byte (expected: {!r}, received: {!r})'.format(
                    self._sync, sync_bytes))

    def _verify_content_length(self, content_length_bytes):
        """Verify and unpack the message content length field.

        :param bytes content_length_bytes: content length bytes.
        :returns: unpacked content length.
        """
        try:
            content_length = unpack(self._field_fmt, content_length_bytes)[0]
        except Exception as ex:
            raise MessageEnvelopeError(
                'Wrong content length byte (expected: {!r}, '
                'received: {!r})'.format(self._field_fmt, content_length_bytes),
                inner=ex)
        return content_length

    def _verify_message_id(self, message_id_bytes):
        """Verify and unpack the message ID field.

        :param bytes message_id_bytes: message ID bytes.
        :returns: unpacked message ID.
        """
        try:
            message_id = unpack(self._field_fmt, message_id_bytes)[0]
        except Exception as ex:
            raise MessageEnvelopeError(
                'Wrong message ID byte (expected: {!r}, '
                'received: {!r})'.format(self._field_fmt,
                                         message_id_bytes),
                inner=ex)
        return message_id

    def _verify_content(self, content, expected_length):
        """Verify message content.

        :param bytes content: content bytes.
        :param int expected_length: expected content length.
        """
        if not content or not hasattr(content, '__len__'):
            raise MessageEnvelopeError('Invalid content')
        elif len(content) != expected_length:
            raise MessageEnvelopeError(
                'Wrong content length (expected: {!r}, received: {!r})'.format(
                    expected_length, len(content)))

    def _verify_checksum(self, checksum_bytes, complete_message):
        """Verify message checksum.

        :param bytes checksum_bytes: checksum bytes.
        :param bytes complete_message: message bytes up to the checksum.
        """
        try:
            checksum = unpack(self._field_fmt, checksum_bytes)[0]
        except Exception as ex:
            raise MessageEnvelopeError('Invalid checksum byte', inner=ex)
        else:
            # Verify the checksum.
            expected_checksum = self._checksum(complete_message)
            if checksum != expected_checksum:
                raise MessageEnvelopeError(
                    'Invalid message checksum (expected: {!r}, '
                    'received: {!r})'.format(
                    expected_checksum, checksum_bytes))

gsm = getGlobalSiteManager()
gsm.registerUtility(Factory(ANTEnvelope, 'ANTEnvelope'),
                    IMessageEnvelopeFactory,
                    'ant')
