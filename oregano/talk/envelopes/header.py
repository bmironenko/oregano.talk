"""Message envelope implemented as a header structure prepended to the payload.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'MessageHeader'
]

import logging
from struct import pack, unpack, calcsize
from zope.interface import implementer
from zope.component import getGlobalSiteManager
from zope.component.factory import Factory
from ..interfaces import *
from ..exceptions_ import *


@implementer(IMessageEnvelope)
class MessageHeader(object):
    """Simple message envelope. Adds a header consisting of:

    - sentinel
    - message size
    """
    _DEFAULT_SENTINEL = b'\x02\03'
    _DEFAULT_SIZE_FMT = '=H'

    _logger = logging.getLogger(__name__ + '.MessageHeader')

    def __init__(self, sentinel=_DEFAULT_SENTINEL, size_fmt=_DEFAULT_SIZE_FMT):
        """Initialize a new MessageHeader instance.

        :param bytes sentinel: override message sentinel byte sequence.
        :param str size_fmt: packing format for the message size field (see \
            the `struct` module documentation).
        :exception ValueError: `sentinel` is not an instance of \
            :py:class:`bytes`, :py:class:`bytearray`, or :py:class:`memoryview`.
        """
        self._logger.debug('Creating with: sentinel=%r, size_fmt=%r',
                           sentinel, size_fmt)
        if not isinstance(sentinel, (bytes, bytearray, memoryview)):
            error = 'Sentinel must be an instance of bytes, bytearray, or ' \
                    'memoryview'
            self._logger.error(error)
            raise ValueError(error)
        self._sentinel = sentinel
        self._size_fmt = size_fmt
        self._calc_size_bytes = calcsize(self._size_fmt)

    def wrap(self, message):
        """Prepend the header to the message.

        :param bytes message: message to wrap.
        :return: header + message data, ready to be sent.
        """
        if not isinstance(message, (bytes, bytearray, memoryview)):
            raise ValueError('\'message\' must implement the buffer protocol')
        return self._sentinel + pack(self._size_fmt, len(message)) + message

    def unwrap(self):
        """Receive a message. This is a generator yields instructions to the
        client. Each instruction is a *(more, what)* tuple, and can be
        one of:

        - (:const:`True`, :py:class:`int`): caller should read this many bytes;
        - (:const:`True`, :py:class:`bytes`): caller should read until the \
          specified byte sequence;
        - (:const:`False`, :py:class:`bytes`): done; complete message is \
          :py:class:`bytes`; next call to the generator will result in a \
          :py:class:`StopIteration`.

        Caller should send the sequence of bytes read back to the generator.
        """
        # Request and verify the sentinel.
        sentinel = yield True, self._sentinel
        if not sentinel or \
                not hasattr(sentinel, '__getitem__') or \
                sentinel[-len(self._sentinel):] != self._sentinel:
            raise MessageEnvelopeError(
                'Wrong sentinel (expected: {!r}, received: {!r})'.format(
                    self._sentinel, sentinel))

        # Request and verify message size bytes.
        size_bytes = yield True, self._calc_size_bytes
        try:
            size = unpack(self._size_fmt, size_bytes)[0]
        except Exception as ex:
            raise MessageEnvelopeError(
                'Wrong size bytes (expected: {!r}, received: {!r})'.format(
                    self._size_fmt, size_bytes), inner=ex)

        # Request and verify (size) the message.
        message = yield True, size
        if not message or not hasattr(message, '__len__'):
            raise MessageEnvelopeError('Invalid message')
        elif len(message) != size:
            raise MessageEnvelopeError(
                'Wrong message size (expected: {!r}, received: {!r})'.format(
                    size, len(message)))

        yield False, message


gsm = getGlobalSiteManager()
gsm.registerUtility(Factory(MessageHeader, 'MessageHeader'),
                    IMessageEnvelopeFactory,
                    'header')
