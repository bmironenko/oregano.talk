"""ANTEnvelope tests.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import re
from struct import pack, calcsize
from ...exceptions_ import *
from ..ant import ANTEnvelope
from ....test import *


class ANTEnvelopeTest(unittest.TestCase):
    """Test the ANTEnvelope class.
    """
    def setUp(self):
        """Set up a test.
        """
        self._envelope = ANTEnvelope()

    def _test_bad_sync(self, bad_sync):
        unwrap = self._envelope.unwrap()
        next(unwrap)
        self.assertRaisesRegex(
            MessageEnvelopeError,
            re.escape(
                'Wrong SYNC byte (expected: {!r}, received: {!r})'.format(
                    self._envelope._sync,  bad_sync)),
            unwrap.send, bad_sync
        )

    def _test_bad_content_length(self, bad_content_length_bytes):
        unwrap = self._envelope.unwrap()
        next(unwrap)
        unwrap.send(self._envelope._sync)
        self.assertRaisesRegex(
            MessageEnvelopeError,
            re.escape('Wrong content length byte (expected: {!r}, '
                      'received: {!r})'.format(self._envelope._field_fmt,
                                               bad_content_length_bytes)),
            unwrap.send, bad_content_length_bytes)

    def _test_bad_message_id(self, bad_message_id_bytes):
        unwrap = self._envelope.unwrap()
        next(unwrap)
        unwrap.send(self._envelope._sync)
        unwrap.send(b'\x05')
        self.assertRaisesRegex(
            MessageEnvelopeError,
            re.escape('Wrong message ID byte (expected: {!r}, '
                      'received: {!r})'.format(self._envelope._field_fmt,
                                               bad_message_id_bytes)),
            unwrap.send, bad_message_id_bytes)

    def _test_bad_content(self, content, bad_content):
        unwrap = self._envelope.unwrap()
        next(unwrap)
        unwrap.send(self._envelope._sync)
        unwrap.send(pack(self._envelope._field_fmt,
                         len(content) if isinstance(content, bytes) else 5))
        unwrap.send(b'\xFF')  # message ID
        if not isinstance(bad_content, bytes):
            self.assertRaisesRegex(
                MessageEnvelopeError,
                'Invalid content', unwrap.send, bad_content)
        else:
            self.assertRaisesRegex(
                MessageEnvelopeError,
                re.escape('Wrong content length (expected: {!r}, '
                          'received: {!r})'.format(len(content),
                                                   len(bad_content))),
                unwrap.send, bad_content)

    def _test_bad_checksum(self, message_id, content, bad_checksum):
        length_bytes = pack(self._envelope._field_fmt, len(content))
        checksum_data = self._envelope._sync + length_bytes + \
            message_id + content
        unwrap = self._envelope.unwrap()
        next(unwrap)
        unwrap.send(self._envelope._sync)
        unwrap.send(pack(self._envelope._field_fmt, len(content)))
        unwrap.send(message_id)
        unwrap.send(content)
        expected_checksum = self._envelope._checksum(checksum_data)
        if not isinstance(bad_checksum, bytes) or len(bad_checksum) != 1:
            self.assertRaisesRegex(
                MessageEnvelopeError,
                'Invalid checksum byte', unwrap.send, bad_checksum)
        else:
            self.assertRaisesRegex(
                MessageEnvelopeError,
                re.escape('Invalid message checksum (expected: {!r}, '
                          'received: {!r})'.format(expected_checksum,
                                                   bad_checksum)),
                unwrap.send, bad_checksum)

    def test_checksum(self):
        """Test the ANTEnvelope._checksum() method with normal input.
        """
        self.assertEqual(self._envelope._checksum(b'ABCDE'), 65)

    def test_checksum_empty_sequence(self):
        """Test the ANTEnvelope._checksum() method with empty input.
        """
        self.assertEqual(self._envelope._checksum(b''), 0)

    def test_checksum_None(self):
        """Test the ANTEnvelope._checksum() method with input that is None.
        """
        self.assertEqual(self._envelope._checksum(None), 0)

    def test_wrap(self):
        """Test the ANTEnvelope.wrap() method with normal input.
        """
        self.assertEqual(self._envelope.wrap(b'\xFFABCDE'),
                         b'\xA4\x05\xFFABCDE\x1F')

    def test_wrap_empty(self):
        """Test the ANTEnvelope.wrap() method with empty input.
        """
        self.assertRaisesRegex(ValueError,
                               'ANT message must include a message ID byte',
                               self._envelope.wrap, b'')

    def test_wrap_None(self):
        """Test the ANTEnvelope.wrap() method with input that is None.
        """
        self.assertRaisesRegex(ValueError,
                               '\'message\' must implement the buffer protocol',
                               self._envelope.wrap, None)

    def test_unwrap(self):
        """Test basic ANTEnvelope.unwrap() usage (normal input).
        """
        message_id, content, checksum = b'\x6F', b'hello', b'\xAC'
        unwrap = self._envelope.unwrap()
        self.assertEqual(next(unwrap), (True, self._envelope._sync))
        # Send the SYNC byte - have 1 byte (length) requested.
        self.assertEqual(unwrap.send(self._envelope._sync), (True, 1))
        # Send the length byte - have 1 byte (message ID) requested.
        self.assertEqual(unwrap.send(pack('=B', len(content))),
                         (True, len(message_id)))
        # Send the message ID - have 2 bytes (content) requested.
        self.assertEqual(unwrap.send(message_id), (True, len(content)))
        # Send the content = have 1 byte (checksum) requested.
        self.assertEqual(unwrap.send(content), (True, len(checksum)))
        # Send the checksum - DONE.
        self.assertEqual(unwrap.send(checksum), (False, message_id + content))

    def test_unwrap_non_matching_sync(self):
        """Test basic ANTEnvelope.unwrap() usage when a non-matching
        SYNC byte is sent by the client.
        """
        self._test_bad_sync(b'\x02\x01\x01')

    def test_unwrap_none_sync(self):
        """Test basic ANTEnvelope.unwrap() usage when a SYNC that is
        None is sent by the client.
        """
        self._test_bad_sync(None)

    def test_unwrap_bad_sync(self):
        """Test basic ANTEnvelope.unwrap() usage when a SYNC of invalid
        type is sent by the client.
        """
        self._test_bad_sync(123)

    def test_unwrap_non_matching_content_length(self):
        """Test basic ANTEnvelope.unwrap() usage when an invalid content
        length input is sent by the client (e.g. 4 bytes instead of 1).
        """
        self._test_bad_content_length(pack('<H', 5))

    def test_unwrap_none_content_length(self):
        """Test basic ANTEnvelope.unwrap() usage when content byte input of None
        is sent by the client.
        """
        self._test_bad_content_length(None)

    def test_unwrap_bad_content_length(self):
        """Test basic ANTEnvelope.unwrap() usage when the content length of a
        wrong type is sent by the client.
        """
        self._test_bad_content_length(123)

    def test_unwrap_non_matching_message_id(self):
        """Test basic ANTEnvelope.unwrap() usage when an invalid message ID
        input is sent by the client (e.g. 4 bytes instead of 1).
        """
        self._test_bad_message_id(pack('<H', 5))

    def test_unwrap_none_message_id(self):
        """Test basic ANTEnvelope.unwrap() usage when a message ID byte input
        of None is sent by the client.
        """
        self._test_bad_message_id(None)

    def test_unwrap_bad_message_id(self):
        """Test basic ANTEnvelope.unwrap() usage when the message ID byte of a
        wrong type is sent by the client.
        """
        self._test_bad_message_id(123)

    def test_unwrap_non_matching_content(self):
        """Test basic ANTEnvelope.unwrap() usage when content of invalid
        length is sent by the client.
        """
        self._test_bad_content(b'abc', b'abcd')

    def test_unwrap_none_content(self):
        """Test basic ANTEnvelope.unwrap() usage when content that is None
        is sent by the client.
        """
        self._test_bad_content(2, None)

    def test_unwrap_bad_content(self):
        """Test basic MessageHeader.unwrap() usage when content of invalid
        type is sent by the client.
        """
        self._test_bad_content(2, 123)

    def test_unwrap_non_matching_checksum(self):
        """Test basic ANTEnvelope.unwrap() usage when checksum of invalid
        length is sent by the client.
        """
        self._test_bad_checksum(b'\x4A', b'\x00', b'\xAB\xEF')

    def test_unwrap_none_checksum(self):
        """Test basic ANTEnvelope.unwrap() usage when checksum that is None
        is sent by the client.
        """
        self._test_bad_checksum(b'\x4A', b'\x00', None)

    def test_unwrap_bad_checksum(self):
        """Test basic MessageHeader.unwrap() usage when checksum of invalid
        type is sent by the client.
        """
        self._test_bad_checksum(b'\x4A', b'\x00', b'\xAB')