"""MessageHeader envelope tests.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import re
from struct import pack, calcsize
from ...exceptions_ import *
from ..header import MessageHeader
from ....test import *


class MessageHeaderTest(unittest.TestCase):
    """Test the MessageHeader class.
    """
    def _test_bad_sentinel(self, sentinel, bad_sentinel):
        fmt, data = '<B', b'abc'
        unwrap = MessageHeader(sentinel, fmt).unwrap()
        next(unwrap)
        self.assertRaisesRegex(
            MessageEnvelopeError,
            re.escape(
                'Wrong sentinel (expected: {!r}, received: {!r})'.format(
                    sentinel,  bad_sentinel)),
            unwrap.send, bad_sentinel
        )

    def _test_bad_size_bytes(self, fmt, bad_size_bytes):
        sentinel = b'\x02\x01'
        unwrap = MessageHeader(sentinel, fmt).unwrap()
        next(unwrap)
        unwrap.send(sentinel)
        self.assertRaisesRegex(
            MessageEnvelopeError,
            re.escape('Wrong size bytes (expected: {!r}, '
                      'received: {!r})'.format(fmt, bad_size_bytes)),
            unwrap.send, bad_size_bytes)

    def _test_bad_message(self, message, bad_message):
        sentinel, fmt, data = b'\x02\x01', '<B', message
        unwrap = MessageHeader(sentinel, fmt).unwrap()
        next(unwrap)
        unwrap.send(sentinel)
        unwrap.send(pack(fmt,
                         len(message) if isinstance(message, bytes) else 5))
        self.assertRaisesRegex(
            MessageEnvelopeError, 'Invalid message', unwrap.send, bad_message)

    def test_init(self):
        """Test MessageHeader initialization.
        """
        sentinel, fmt = b'\x02', '@B'
        header = MessageHeader(sentinel, fmt)
        self.assertEqual(header._sentinel, sentinel)
        self.assertEqual(header._size_fmt, fmt)

    def test_init_defaults(self):
        """Test MessageHeader initialization.
        """
        header = MessageHeader()
        self.assertEqual(header._sentinel, header._DEFAULT_SENTINEL)
        self.assertEqual(header._size_fmt, header._DEFAULT_SIZE_FMT)

    def test_init_invalid_sentinel(self):
        """Test MessageHeader initialization with an invalid sentinel value.
        """
        self.assertRaisesRegex(ValueError,
                               'Sentinel must be an instance of bytes, '
                               'bytearray, or memoryview',
                               MessageHeader, mock.MagicMock())

    def test_wrap_None(self):
        """Test the MessageHeader.wrap() method with input that is None.
        """
        self.assertRaisesRegex(ValueError,
                               '\'message\' must implement the buffer protocol',
                               MessageHeader().wrap, None)

    def test_wrap_big_endian_short(self):
        """Test MessageHeader.wrap() with a big-endian format.
        """
        sentinel, fmt = b'\x03\x12', '>H'
        message = b'abc'
        header = MessageHeader(sentinel, fmt)
        wrapped = header.wrap(message)
        self.assertEqual(wrapped, sentinel + pack(fmt, len(message)) + message)

    def test_wrap_little_endian_int(self):
        """Test MessageHeader.wrap() with a big-endian format.
        """
        sentinel, fmt = b'\x13\x10', '<I'
        message = b'abcd' * 1000
        header = MessageHeader(sentinel, fmt)
        wrapped = header.wrap(message)
        self.assertEqual(wrapped, sentinel + pack(fmt, len(message)) + message)

    def test_unwrap(self):
        """Test basic MessageHeader.unwrap() usage.
        """
        sentinel, fmt, data = b'\x02\x02', '>I', b'abc'
        size = pack(fmt, len(data))
        unwrap = MessageHeader(sentinel, fmt).unwrap()
        self.assertEqual(next(unwrap), (True, sentinel))
        self.assertEqual(unwrap.send(sentinel), (True, calcsize(fmt)))
        self.assertEqual(unwrap.send(size), (True, len(data)))
        self.assertEqual(unwrap.send(data), (False, data))

    def test_unwrap_non_matching_sentinel(self):
        """Test basic MessageHeader.unwrap() usage when a non-matching
        sentinel is sent by the client.
        """
        self._test_bad_sentinel(b'\x02\x01', b'\x02\x01\x01')

    def test_unwrap_none_sentinel(self):
        """Test basic MessageHeader.unwrap() usage when a sentinel that is
        None is sent by the client.
        """
        self._test_bad_sentinel(b'\x02\x01', None)

    def test_unwrap_bad_sentinel(self):
        """Test basic MessageHeader.unwrap() usage when a sentinel of invalid
        type is sent by the client.
        """
        self._test_bad_sentinel(b'\x02\x01', 123)

    def test_unwrap_non_matching_size_bytes(self):
        """Test basic MessageHeader.unwrap() usage when an invalid size
        input is sent by the client (e.g. 4 bytes instead of 2).
        """
        self._test_bad_size_bytes('<B', pack('<H', 5))

    def test_unwrap_none_size_bytes(self):
        """Test basic MessageHeader.unwrap() usage when size byte input of None
        is sent by the client.
        """
        self._test_bad_size_bytes('<B', None)

    def test_unwrap_non_matching_message(self):
        """Test basic MessageHeader.unwrap() usage when a message of invalid
        size is sent by the client.
        """
        sentinel, fmt, data, bad_data = b'\x02\x01', '<B', b'abc', b'abcd'
        unwrap = MessageHeader(sentinel, fmt).unwrap()
        next(unwrap)
        unwrap.send(sentinel)
        unwrap.send(pack(fmt, len(data)))
        self.assertRaisesRegex(
            MessageEnvelopeError,
            re.escape('Wrong message size (expected: {!r}, '
                      'received: {!r})'.format(
                      len(data), len(bad_data))),
            unwrap.send, bad_data)

    def test_unwrap_none_message(self):
        """Test basic MessageHeader.unwrap() usage when a message that is None
        is sent by the client.
        """
        self._test_bad_message(2, None)

    def test_unwrap_bad_message(self):
        """Test basic MessageHeader.unwrap() usage when a message of invalid
        type is sent by the client.
        """
        self._test_bad_message(2, 123)
