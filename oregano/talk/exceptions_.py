"""Common exceptions.
"""

from __future__ import absolute_import
from ..util import LoggingMixin

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

__all__ = [
    'TransportError',
    'MessageEnvelopeError',
    'ChannelError',
    'ChannelTimeoutError',
]


class _BaseError(Exception, LoggingMixin):
    """Base exception class for custom errors.
    """
    def __init__(self, message=None, inner=None):
        """Initialize a new exception instance.

        :param message: exception message.
        :param inner: inner exception (implementation-specific). If `message` \
        was not provided but a `inner` was, its string representation is used.
        """
        self.logger.debug('Creating with: message=%r, inner=%r', message, inner)
        self.inner = inner
        self.message = message
        if not message and inner:
            self.message = str(inner)
        super(_BaseError, self).__init__(self.message, inner)

    def __str__(self):
        """Return the string representation of the error.
        """
        return self.message


class TransportError(_BaseError):
    """Generic transport error. Wraps any lower-level exceptions
    (e.g. :py:class:`socket.error`).
    """


class MessageEnvelopeError(_BaseError):
    """Raised by :class:`IMessageEnvelope` instance to indicate invalid envelope
    parsing state; for example, a client sending 3 bytes when 2 were requested
    by the unwrap operation. Any lower-level exceptions raised during envelope
    processing will also be wrapped by a *MessageEnvelopeError*.
    """


class ChannelError(_BaseError):
    """Raised by :class:`IChannel` instance to indicate a generic channel error.
    """


class ChannelTimeoutError(ChannelError):
    """Raised by :class:`IChannel` instance to indicate a timeout when receiving
    a complete message
    """