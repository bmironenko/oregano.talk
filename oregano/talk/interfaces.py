"""Communication interfaces and generic functionality.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'IConnection',
    'ITransportConf',
    'ITransportConfFactory',
    'IFileDescriptor',
    'ITransport',
    'IChannelConf',
    'IChannelConfFactory',
    'IChannel',
    'IMessageEnvelope',
    'IMessageEnvelopeFactory',
]

from zope.interface import Interface, Attribute
from zope.component.interfaces import IFactory


class IConnection(Interface):  # pragma: no cover
    """Generic connection interface. Marks an object that provides connection
    semantics. If a transport instance provides :class:`IConnection`,
    for example, any channel using the transport will invoke its
    :func:`connect()` method prior to using it.
    """
    connected = Attribute('Current state',
                          'Indicates the state of the connection '
                          '(:const:`True` or :const:`False`).')

    def connect():
        """Establish the connection.
        """

    def close():
        """Close the connection.
        """


class IFileDescriptor(Interface):  # pragma: no cover
    """Transports needing to expose their underlying file descriptor should
    implement this interface.
    """
    fileno = Attribute('File descriptor', 'File descriptor value '
                                          '(:py:class:`int`).')


class ITransportConf(Interface):  # pragma: no cover
    """Interface for classes that provide generic transport configuration.
    """
    blocking = Attribute('Blocking flag',
                         'Set or return blocking mode '
                         '(:const:`True` or :const:`False`).')
    write_timeout = Attribute('Write timeout',
                              'Write timeout, in seconds, for blocking mode.')
    read_timeout = Attribute('Read timeout',
                             'Read timeout, in seconds, for blocking mode.')


class ITransportConfFactory(IFactory):  # pragma: no cover
    """Interface for registering :class:`ITransportConf` factories.
    """


class ITransport(Interface):  # pragma: no cover
    """Transport interface. Defines low-level read/write access.
    """
    conf = Attribute('Transport configuration',
                     ':class:`ITransportConf` instance that configures this '
                     'transport.')

    def write(data):
        """Write data to the transport.

        :param bytes data: data to write (must implement the buffer protocol).
        :return: number of bytes written.
        """

    def read(count=None):
        """Read data from the transport.

        :param int count: number of bytes to read. Blocking transport reads \
            will block until timed out (if read timeout is configured) or the \
            amount of data specified is available. When :const:`None`, return \
            whatever data is available.
        :return: data received (buffer), or :const:`None` if no data is \
            available.
        """

    def read_into(buffer):
        """Read data from the transport into a buffer.

        :param bytes buffer: byte array to populate instead of returning data. \
            This can be a memoryview created with an offset, for example. The \
            length of the buffer specifies the number of bytes to read. \
            Blocking transport reads will block until timed out (if read \
            timeout is configured) or the amount of data specified is \
            available. When :const:`None`, return whatever data is available.
        :return: number of bytes stored in the buffer.
        """


class IChannelConf(Interface):  # pragma: no cover
    """Interface for classes that provide generic channel configuration.
    """
    transport_conf = Attribute(
        'Transport configuration',
        ':class:`ITransportConf` instance specifying the transport to use with '
        'this channel.'
    )
    message_envelope = Attribute(
        'Message envelope',
        ':class:`IMessageEnvelope` instance for wrapping/unwrapping messages.'
    )
    receive_timeout = Attribute(
        'Receive timeout',
        'Return or set the message receive timeout, in seconds.')


class IChannelConfFactory(IFactory):  # pragma: no cover
    """Interface for registering :class:`IChannelConf` factories.
    """


class IChannel(Interface):  # pragma: no cover
    """Message channel. Handles message boundaries. Does not implement
    request-response semantics; that is the job of the higher :class:`ISession`
    layer. Channel behavior depends on whether the underlying transport is
    configured in non-blocking mode. When it is, :func:`send()` and
    :func:`receive()` are asynchronous and return
    :class:`concurrent.futures.Future` instances.
    """
    conf = Attribute('Channel configuration',
                     ':class:`IChannelConf` instance that configures this '
                     'channel.')

    def send(message):
        """Send a message, wrapping it in an envelope/header.

        :param bytes message: message to send.
        :return: if the underlying transport is non-blocking, a \
            :py:class:`concurrent.futures.Future` object. If the transport is \
            blocking, :const:`True` if the message was successfully sent and \
            :const:`False` otherwise (though, in this case, an exception will \
            most likely be raised).
        """

    def receive():
        """Receive the next message.

        :return: message received, or a :py:class:`concurrent.futures.Future` \
            instance if the underlying transport is non-blocking.
        """


class IMessageEnvelope(Interface):  # pragma: no cover
    """Message envelope. Knows how to wrap a message prior to sending it; knows
    how to recognize a message when receiving byte data.
    """
    def wrap(message):
        """Wrap the message prior to sending it.

        :param bytes message: message to wrap.
        :return: wrapped message data, ready to be sent.
        """

    def unwrap():
        """Receive a message. This is a generator that yields instructions to \
        the client. Each instruction is a *(more, what)* tuple, and can be \
        one of:

        - (:const:`True`, :py:class:`int`): caller should read this many bytes;
        - (:const:`True`, :py:class:`bytes`): caller should read until the \
          specified byte sequence;
        - (:const:`False`, :py:class:`bytes`): done; complete message is \
          :py:class:`bytes`; next call to the generator will result in a \
          :py:class:`StopIteration`.

        Caller should send the sequence of bytes read back to the generator.
        """


class IMessageEnvelopeFactory(IFactory):  # pragma: no cover
    """Interface for registering :class:`IMessageEnvelope` factories as a
    zope.component utility.
    """


class ISession(Interface):  # pragma: no cover
    """Provides request/response semantics. Multiple sessions may wrap the same
    channel. For example, if a device is accessed via a web API, multiple
    clients may create individual sessions around the same global channel.
    Responses are routed to the same session that generated the corresponding
    request.
    """
    def send_request(body):
        """Send a request and receive a matching response.

        :param bytes body: request body.
        :return: response, or :const:`None` if a timeout occurs. If the \
            underlying transport is non-blocking, returns a \
            :class:`concurrent.futures.Future` instance instead.
        """

    def on_request(body):
        """Invoked when a request message is received.

        :return: response (buffer), which is then sent back over the channel.
        """


class ISessionFactory(IFactory):  # pragma: no cover
    """Creates and manages :class:`ISession` instances.
    """


class IMessageEncoder(Interface):  # pragma: no cover
    """Message encoder. Converts a message to a byte sequence.
    """
    def encode(message):
        """Encode the specified message.

        :param bytes message: message payload.
        :return: message as a byte sequence.
        """


class IMessageDecoder(Interface):  # pragma: no cover
    """Message decoder. Convers a byte sequence to an object it is supposed
    to represent.
    """
    def decode(raw_bytes):
        """Decode the specified byte sequence.

        :param bytes raw_bytes: message as a byte sequence.
        :return: message object.
        """
