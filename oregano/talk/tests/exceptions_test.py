"""General interface and common base class tests.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

from ..exceptions_ import _BaseError
from ...test import *


class BaseErrorTest(unittest.TestCase):
    """Test the _BaseError exception class.
    """
    def test_init_with_message_only(self):
        """Test _BaseError initialization with the message parameter only.
        """
        e = _BaseError('abc')
        self.assertEqual(str(e), 'abc')
        self.assertEqual(repr(e), '_BaseError(\'abc\', None)')
        self.assertEqual(e.message, 'abc')
        self.assertIsNone(e.inner)

    def test_init_with_inner_only(self):
        """Test WrappedError initialization with the inner parameter only.
        """
        v = ValueError('abc')
        e = _BaseError(inner=v)
        self.assertEqual(str(e), str(v))
        self.assertEqual(e.message, str(v))
        self.assertEqual(e.inner, v)

    def test_init_with_message_and_inner(self):
        """Test WrappedError initialization with both parameters.
        """
        v = ValueError('abc')
        e = _BaseError('def', inner=v)
        self.assertEqual(str(e), 'def')
        self.assertEqual(e.message, 'def')
        self.assertEqual(e.inner, v)


if __name__ == '__main__':
    unittest.main()
