"""Serial transport functionality.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'SerialTransportConf',
    'SerialTransport',
]

import warnings
import serial
from zope.interface import Attribute, implementer
from zope.component import adapter
from zope.component.factory import Factory
from zope.component import getGlobalSiteManager
from ..interfaces import *
from ..exceptions_ import *
from ..warnings import *
from ...util import LoggingMixin


class _ISerialTransportConf(ITransportConf):  # pragma: no cover
    """Marker interface for the :class:`SerialTransportConf` class; exists for
    adapter registration purposes.
    """
    port = Attribute('Port descriptor', 'Serial port descriptor string.')
    baud_rate = Attribute('Baud rate', 'Serial port baud rate.')


@implementer(_ISerialTransportConf)
class SerialTransportConf(LoggingMixin):
    """Configuration for a serial transport.
    """
    BAUD_RATES = (
        300, 600, 1200, 2400, 4800, 9600, 14400,
        19200, 28800, 38400, 57600, 115200
    )

    def __init__(self, port, baud_rate, blocking=False,
                 write_timeout=None, read_timeout=None):
        """Initialize a new serial transport configuration object.

        :param str port: serial port descriptor string (e.g. file name).
        :param int baud_rate: serial port baud rate to use.
        :param bool blocking: select blocking/non-blocking I/O operation.
        :param int write_timeout: blocking write timeout.
        :param int read_timeout: blocking read timeout.
        :exception ValueError: invalid `baud_rate` specified.
        """
        self.logger.debug(
            'Creating with: port=%r, baud_rate=%r, blocking=%r, '
            'write_timeout=%r, read_timeout=%r', port, baud_rate, blocking,
            write_timeout, read_timeout)
        if baud_rate not in SerialTransportConf.BAUD_RATES:
            message = 'Invalid baud rate'
            self.logger.error(message)
            raise ValueError(message)
        self._port = port
        self._baud_rate = baud_rate
        self._blocking = blocking
        if not self._blocking:
            if write_timeout or read_timeout:
                message = 'I/O timeouts are ignored in non-blocking ' \
                          'transport mode'
                self.logger.warning(message)
                warnings.warn(message, TransportConfWarning)
            self._write_timeout = self._read_timeout = 0
        else:
            if write_timeout == 0:
                message = 'Changing write timeout value of 0 to None. ' \
                          'Did you intend to use non-blocking I/O?'
                self.logger.warning(message)
                warnings.warn(message, TransportConfWarning)
            if read_timeout == 0:
                message = 'Changing read timeout value of 0 to None. ' \
                          'Did you intend to use non-blocking I/O?'
                self.logger.warning(message)
                warnings.warn(message, TransportConfWarning)
            self._write_timeout = None if not write_timeout else write_timeout
            self._read_timeout = None if not read_timeout else read_timeout

    @property
    def port(self):
        """Return serial port descriptor (e.g. '*/dev/tty-usbmodemfa141*').
        """
        return self._port

    @property
    def baud_rate(self):
        """Return serial port baud rate.
        """
        return self._baud_rate

    @property
    def blocking(self):
        """Return :const:`True` if blocking operation is configured.
        """
        return self._blocking

    @property
    def write_timeout(self):
        """Return the blocking write timeout, in seconds.
        """
        return self._write_timeout

    @property
    def read_timeout(self):
        """Return the blocking read timeout, in seconds.
        """
        return self._read_timeout

    def __repr__(self):
        """Return a string representation of this configuration object.
        """
        return 'SerialTransportConf(port={!r}, baud_rate={!r}, ' \
               'blocking={!r}, write_timeout={!r}, read_timeout={!r})'.format(
               self.port, self.baud_rate, self.blocking,
               self.write_timeout, self.read_timeout)

    def __eq__(self, other):
        """Test two objects for equality.
        """
        if not isinstance(other, self.__class__):
            return False
        elif self is other:
            return True
        return \
            self.port == other.port and \
            self.baud_rate == other.baud_rate and \
            self.blocking == other.blocking and \
            self.write_timeout == other.write_timeout and \
            self.read_timeout == other.read_timeout


@implementer(ITransport)
@implementer(IFileDescriptor)
@implementer(IConnection)
@adapter(_ISerialTransportConf)
class SerialTransport(LoggingMixin):
    """Implements a serial transport.
    """
    # TODO: handle 'OSError: [Errno 6] Device not configured' (device goes away while port is open)

    def __init__(self, conf):
        """Initialize a new serial transport.

        :param conf: :class:`SerialTransportConf` to use to configure this \
            transport.
        :exception ValueError: configuration object is not an instance of \
            :class:`SerialTransportConf`.
        """
        self.logger.debug('Creating with: conf=%r', conf)
        if not _ISerialTransportConf.providedBy(conf):
            message = 'Configuration object must be an ' \
                      'instance of SerialTransportConf'
            self.logger.error(message)
            raise ValueError(message)
        self._conf = conf
        self._serial_port = serial.Serial()

    @property
    def conf(self):
        """Return the :class:`SerialTransportConf` instance that configures this
        transport.
        """
        return self._conf

    @property
    def connected(self):
        """Return :const:`True` if the transport is open/ready; otherwise,
        return False.
        """
        return self._serial_port.isOpen()

    @property
    def fileno(self):
        """Return the underlying file descriptor.
        """
        if self.connected:
            return self._serial_port.fileno()

    def connect(self):
        """Open the transport.

        :exception oregano.talk.interfaces.TransportError: wraps the \
            :py:class:`serial.SerialException` raised by the underlying open \
            operation.
        """
        self._serial_port.port = self.conf.port
        self._serial_port.baudrate = self.conf.baud_rate
        self._serial_port.writeTimeout = self.conf.write_timeout
        self._serial_port.timeout = self.conf.read_timeout
        try:
            self.logger.debug('Connecting to %r', self.conf.port)
            self._serial_port.open()
        except serial.SerialException as ex:
            self.logger.exception(ex)
            raise TransportError(inner=ex)

    def close(self):
        """Close the transport. A closed serial transport may be reopened later.

        :exception oregano.talk.interfaces.TransportError: wraps the \
            :py:class:`serial.SerialException` raised by the underlying close \
            operation.
        """
        try:
            self.logger.debug('Closing %r', self.conf.port)
            self._serial_port.close()
        except serial.SerialException as ex:
            self.logger.exception(ex)
            raise TransportError(inner=ex)

    def write(self, data):
        """Write data to the transport.

        :param bytes data: data to write (must implement the buffer protocol).
        :return: number of bytes written.
        :exception ValueError: `data` is not :py:class:`bytes`, \
            :py:class:`bytearray`, or :py:class:`memoryview`, i.e. does not \
            implement the buffer protocol.
        :exception oregano.talk.interfaces.TransportError: transport is not \
            connected; can also wrap a :py:class:`serial.SerialException` \
            raised by the underlying write operation.
        """
        self.logger.debug('Writing to %r', self.conf.port)
        if not isinstance(data, (bytes, bytearray, memoryview)):
            message = '\'data\' must implement the buffer protocol'
            self.logger.error(message)
            raise ValueError(message)
        try:
            if not self.connected:
                raise TransportError('Transport is closed')
            return self._serial_port.write(data)
        except serial.SerialException as ex:
            self.logger.exception(ex)
            raise TransportError(inner=ex)

    def read(self, count=None):
        """Read data from the transport.

        :param int count: number of bytes to read. Blocking transport reads \
            will block until timed out (if read timeout is configured) or the \
             amount of data specified is available. When :const:`None`, return \
             whatever data is available.
        :return: data received (buffer).
        :exception oregano.talk.interfaces.TransportError: transport is not \
            connected; can also wrap a :py:class:`serial.SerialException` \
            raised by the underlying read operation.
        """
        self.logger.debug('Reading %r bytes from %r', count, self.conf.port)
        try:
            if not self.connected:
                raise TransportError('Transport is closed')
            in_waiting = self._serial_port.inWaiting()
            self.logger.debug('%r bytes available', in_waiting)
            if in_waiting:
                return self._serial_port.read(count or in_waiting)
        except serial.SerialException as ex:
            self.logger.exception(ex)
            raise TransportError(inner=ex)

    def read_into(self, buffer):
        """Read data from the transport.

        :param bytes buffer: byte array to populate instead of returning data. \
            This can be a :py:class:`memoryview` created with an offset, for \
            example. The length of the buffer specifies the number of bytes to \
            read. Blocking transport reads will block until timed out (if read \
            timeout is configured) or the amount of data specified is available.
        :return: number of bytes stored in the buffer.
        """
        self.logger.debug('Reading from %r into a buffer of length %r',
                          self.conf.port, len(buffer))
        bytes_read = self.read(len(buffer))
        if bytes_read:
            buffer[:len(bytes_read)] = bytes_read
        return len(bytes_read) if bytes_read else 0

    def __repr__(self):
        """Return a unique string representation of this object.
        """
        return 'SerialTransport(conf={!r})'.format(self.conf)

    def __eq__(self, other):
        """Test two objects for equality.
        """
        if not isinstance(other, self.__class__):
            return False
        return self.conf == other.conf


gsm = getGlobalSiteManager()
gsm.registerUtility(Factory(SerialTransportConf, 'SerialTransportConf'),
                    ITransportConfFactory, 'serial')
gsm.registerAdapter(SerialTransport, (_ISerialTransportConf,), ITransport, '')
