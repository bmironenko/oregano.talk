"""Serial transport tests.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import serial
from zope.interface.verify import verifyObject
from zope.component import getUtility
from ..serial import *
from ..serial import _ISerialTransportConf
from ....test import *
from ...interfaces import *
from ...exceptions_ import *
from ...warnings import *


class SerialTransportConfTest(unittest.TestCase):
    """Test the serial transport configuration class.
    """
    def _verify_conf(self, conf, port, baud_rate, blocking,
                     write_timeout, read_timeout):
        self.assertTrue(_ISerialTransportConf.providedBy(conf))
        verifyObject(_ISerialTransportConf, conf)
        self.assertEqual(conf.port, port)
        self.assertEqual(conf.baud_rate, baud_rate)
        self.assertEqual(conf.blocking, blocking)
        self.assertEqual(conf.write_timeout, write_timeout)
        self.assertEqual(conf.read_timeout, read_timeout)

    def test_init(self):
        """Initialize a SerialTransportConf instance.
        """
        conf = SerialTransportConf('/dev/tty1', 115200,
                                   blocking=True,
                                   write_timeout=1,
                                   read_timeout=2)
        self._verify_conf(conf, '/dev/tty1', 115200, True, 1, 2)

    def test_init_factory(self):
        """Initialize a SerialTransportConf instance using a factory.
        """
        conf = getUtility(ITransportConfFactory, 'serial')(port='/dev/tty2',
                                                           baud_rate=14400)
        self._verify_conf(conf, '/dev/tty2', 14400, False, 0, 0)

    def test_init_factory_invalid_args(self):
        """Initialize a SerialTransportConf instance using a factory with
        an invalid argument set.
        """
        self.assertRaises(TypeError,
                          getUtility(ITransportConfFactory, 'serial'))

    def test_init_invalid_baud_rate(self):
        """Initialize a SerialTransportConf with an invalid baud rate
        value.
        """
        self.assertRaisesRegex(ValueError, 'Invalid baud rate',
                               SerialTransportConf, '/dev/tty1', 0)

    def test_init_warn_nonblocking_timeouts(self):
        """Initialize a SerialTransportConf with the combination of
        blocking=False and blocking timeouts being set.
        """
        with warnings.catch_warnings(record=True) as triggered:
            warnings.simplefilter("always")
            conf = SerialTransportConf('/dev/tty1', 115200,
                                       blocking=False,
                                       write_timeout=None, read_timeout=2)
            self.assertEqual(len(triggered), 1)
            self.assertIs(triggered[0].category, TransportConfWarning)
            self.assertEqual(triggered[0].message.args[0],
                             'I/O timeouts are ignored in '
                             'non-blocking transport mode')
            self.assertEqual(conf.write_timeout, 0)
            self.assertEqual(conf.read_timeout, 0)

    def test_init_warn_blocking_zero_timeouts(self):
        """Initialize a SerialTransportConf with blocking=False and
        timeouts set to 0. Write timeout should be converted to None.
        """
        with warnings.catch_warnings(record=True) as triggered:
            warnings.simplefilter("always")
            conf = SerialTransportConf('/dev/tty1', 115200,
                                       blocking=True,
                                       write_timeout=0, read_timeout=0)
            self.assertEqual(len(triggered), 2)
            self.assertIs(triggered[0].category, TransportConfWarning)
            self.assertIs(triggered[1].category, TransportConfWarning)
            self.assertEqual(triggered[0].message.args[0],
                             'Changing write timeout value of 0 to None. '
                             'Did you intend to use non-blocking I/O?')
            self.assertEqual(triggered[1].message.args[0],
                             'Changing read timeout value of 0 to None. '
                             'Did you intend to use non-blocking I/O?')
            self.assertIsNone(conf.write_timeout)
            self.assertIsNone(conf.read_timeout)

    def test_repr(self):
        """Test SerialTransportConf.__repr__().
        """
        conf = SerialTransportConf('/dev/tty1', 115200,
                                   blocking=True,
                                   write_timeout=2, read_timeout=4)
        self.assertEqual(conf, eval(repr(conf)))

    def test_eq(self):
        """Test SerialTransportConf.__eq__() when objects are equal.
        """
        confs = []
        for i in range(2):
            confs.append(SerialTransportConf('/dev/tty1', 115200,
                                             blocking=True,
                                             write_timeout=2, read_timeout=4))
        self.assertEqual(confs[0], confs[1])

    def test_eq_same_object(self):
        """Test SerialTransportConf.__eq__() when comparing an object to
        itself.
        """
        conf = SerialTransportConf('/dev/tty1', 115200,
                                   blocking=True,
                                   write_timeout=2, read_timeout=4)
        self.assertEqual(conf, conf)

    def test_eq_different_types(self):
        """Test SerialTransportConf.__eq__() when objects being compared are
        of two different types.
        """
        conf = SerialTransportConf('/dev/tty1', 115200,
                                   blocking=True,
                                   write_timeout=2, read_timeout=4)
        self.assertNotEqual(conf, 'abc')

    def test_eq_not_equal(self):
        """Test SerialTransportConf.__eq__() when objects being compared are
        not equal.
        """
        self.assertNotEqual(
            SerialTransportConf('/dev/tty1', 115200, blocking=True,
                                read_timeout=5, write_timeout=3),
            SerialTransportConf('/dev/tty2', 115200, blocking=True,
                                read_timeout=4, write_timeout=2)
        )


class SerialTransportTest(unittest.TestCase):
    """Test the serial transport class.
    """
    def _create_transport(self, blocking=True, as_adapter=False):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            self.conf = SerialTransportConf(port='/dev/tty1', baud_rate=14400,
                                            blocking=blocking, write_timeout=1,
                                            read_timeout=1)
        with mock.patch('serial.Serial') as m:
            self.transport = ITransport(self.conf) \
                if as_adapter \
                else SerialTransport(self.conf)
            self.Serial = m
            self.serial_port = m.return_value
        self.serial_port.isOpen.return_value = False
        self._verify_new_transport()
        return self.transport

    def _verify_new_transport(self):
        verifyObject(ITransport, self.transport)
        verifyObject(IFileDescriptor, self.transport)
        verifyObject(IConnection, self.transport)
        self.assertEqual(self.transport.conf, self.conf)
        self.assertFalse(self.transport.connected)
        self.Serial.assert_called_once_with()
        self.assertIs(self.transport._serial_port, self.serial_port)
        self.assertIsNone(self.transport.fileno)

    def _open_transport(self):
        self.transport.connect()
        self.serial_port.isOpen.return_value = True
        self.serial_port.fileno.return_value = 11
        self.assertEqual(self.serial_port.port, self.transport.conf.port)
        self.assertEqual(self.serial_port.baudrate,
                         self.transport.conf.baud_rate)
        self.assertEqual(self.serial_port.writeTimeout,
                         self.transport.conf.write_timeout)
        self.assertEqual(self.serial_port.timeout,
                         self.transport.conf.read_timeout)
        self.serial_port.open.assert_called_once_with()
        self.assertTrue(self.transport.connected)
        self.assertEqual(self.transport.fileno, 11)

    def _close_transport(self):
        self.transport.close()
        self.serial_port.isOpen.return_value = False
        self.serial_port.close.assert_called_once_with()
        self.assertFalse(self.transport.connected)

    def test_init(self):
        """Initialize a SerialTransport instance.
        """
        self._create_transport()

    def test_init_adapter(self):
        """Initialize a SerialTransport instance given a conf object using
        the adapter lookup mechanism.
        """
        self._create_transport(as_adapter=True)

    def test_init_invalid_conf(self):
        """Initialize a SerialTransport instance with an invalid conf object.
        """
        self.assertRaisesRegex(ValueError,
                               'Configuration object must be an '
                               'instance of SerialTransportConf',
                               SerialTransport, mock.Mock())

    def test_init_none_conf(self, *mocks):
        """Initialize a SerialTransport instance with a conf object
        that is None.
        """
        self.assertRaisesRegex(ValueError,
                               'Configuration object must be an '
                               'instance of SerialTransportConf',
                               SerialTransport, None)

    def test_open(self):
        """Test SerialTransport.open().
        """
        self._create_transport()
        self._open_transport()

    def test_open_exception(self):
        """Test SerialTransport.open() when the underlying serial port
        object raises an exception. The exception should be wrapped in a
        TransportException.
        """
        self._create_transport()
        exception = serial.SerialException('def')
        self.serial_port.open.side_effect = exception
        try:
            self.transport.connect()
        except TransportError as ex:
            self.assertEqual(ex.message, 'def')
            self.assertEqual(ex.inner, exception)
        else:
            self.fail('Expected a TransportException to be raised')

    def test_close(self):
        """Test SerialTransport.close().
        """
        self._create_transport()
        self._open_transport()
        self._close_transport()

    def test_close_exception(self):
        """Test SerialTransport.close() when the underlying serial port
        object raises an exception. The exception should be wrapped in a
        TransportException.
        """
        self._create_transport()
        exception = serial.SerialException('def')
        self.serial_port.close.side_effect = exception
        self._open_transport()
        try:
            self.transport.close()
        except TransportError as ex:
            self.assertEqual(ex.message, 'def')
            self.assertEqual(ex.inner, exception)
        else:
            self.fail('Expected a TransportException to be raised')

    def test_write_blocking(self):
        """Test SerialTransport.write().
        """
        self._create_transport()
        self.serial_port.write.return_value = 3
        self._open_transport()
        self.assertEqual(self.transport.write(b'abc'), 3)
        self.serial_port.write.assert_called_once_with(b'abc')

    def test_write_string(self):
        """Test SerialTransport.write() with a unicode string.
        """
        self._create_transport()
        self._open_transport()
        self.assertRaisesRegex(ValueError,
                               '\'data\' must implement the buffer protocol',
                               self.transport.write, u'abc')

    def test_write_to_closed(self):
        """Test SerialTransport.write() with a closed transport.
        """
        self._create_transport()
        try:
            self.transport.write(b'data')
        except TransportError as ex:
            self.assertEqual(ex.message, 'Transport is closed')
            self.assertEqual(ex.inner, None)
        else:
            self.fail('Expected a TransportWriteError to be raised')

    def test_write_exception(self):
        """Test SerialTransport.write() where the underlying serial port
        raises a SerialException.
        """
        self._create_transport()
        exception = serial.SerialException('se')
        self.serial_port.write.side_effect = exception
        self._open_transport()
        try:
            self.transport.write(b'data')
        except TransportError as ex:
            self.assertEqual(ex.message, 'se')
            self.assertEqual(ex.inner, exception)
        else:
            self.fail('Expected a TransportWriteError to be raised')

    def test_write_exception(self):
        """Test SerialTransport.write() where the underlying serial port
        raises a SerialTimeoutException.
        """
        self._create_transport()
        exception = serial.SerialTimeoutException('ste')
        self.serial_port.write.side_effect = exception
        self._open_transport()
        try:
            self.transport.write(b'data')
        except TransportError as ex:
            self.assertEqual(ex.message, 'ste')
            self.assertEqual(ex.inner, exception)
        else:
            self.fail('Expected a TransportWriteError to be raised')

    def test_read(self):
        """Test SerialTransport.read(count=None). Should return whatever data
        is available.
        """
        self._create_transport()
        self.serial_port.read.return_value = b'data'
        self.serial_port.inWaiting.return_value = 4
        self._open_transport()
        self.assertEqual(self.transport.read(), b'data')
        self.serial_port.inWaiting.assert_called_once_with()
        self.serial_port.read.assert_called_once_with(4)

    def test_read_into(self):
        """Test SerialTransport.read_into().
        """
        self._create_transport()
        buffer = bytearray(b'abcdef')
        self.serial_port.read.return_value = b'123'
        self.serial_port.inWaiting.return_value = 3
        self._open_transport()
        self.assertEqual(self.transport.read_into(buffer), 3)
        self.serial_port.read.assert_called_once_with(6)
        self.assertEqual(buffer, b'123def')

    def test_read_into_buffer_too_small(self):
        """Test SerialTransport.read_into() with a buffer that is smaller than
        the number of bytes requested to be read.
        """
        self._create_transport()
        self._open_transport()
        buffer = bytearray(b'a')
        self.serial_port.inWaiting.return_value = 3
        self.serial_port.read.return_value = b'1'
        self.transport.read_into(buffer)
        self.serial_port.read.assert_called_once_with(1)
        self.assertEqual(buffer, b'1')

    def test_read_into_more_available(self):
        """Test SerialTransport.read_into() when there is more data available
        than would fit into the buffer, and yet count=None (read all available)
        was specified.
        """
        self._create_transport()
        self._open_transport()
        buffer = bytearray(b'abcdef')
        self.serial_port.read.return_value = b'012345'
        self.serial_port.inWaiting.return_value = 10
        self.assertEqual(self.transport.read_into(buffer), 6)
        self.assertEqual(buffer, b'012345')

    def test_read_into_no_data(self):
        """Test SerialTransport.read() when there is no data available.
        """
        self._create_transport()
        buffer = bytearray(b'abcdef')
        self.serial_port.read.return_value = b''
        self.serial_port.inWaiting.return_value = 0
        self._open_transport()
        self.assertEqual(self.transport.read_into(buffer), 0)
        self.assertEqual(buffer, b'abcdef')

    def test_read_count(self):
        """Test SerialTransport.read() with a non-None count parameter.
        """
        self._create_transport()
        self.serial_port.read.return_value = b'data'
        self._open_transport()
        self.assertEqual(self.transport.read(count=3), b'data')
        self.serial_port.read.assert_called_once_with(3)

    def test_read_from_closed(self):
        """Test SerialTransport.read() with a closed transport.
        """
        self._create_transport()
        try:
            self.transport.read()
        except TransportError as ex:
            self.assertEqual(ex.message, 'Transport is closed')
            self.assertEqual(ex.inner, None)
        else:
            self.fail('Expected a TransportReadError to be raised')

    def test_read_exception(self):
        """Test SerialTransport.read() where a SerialException is raised by
        the underlying port.
        """
        self._create_transport()
        exception = serial.SerialException('se')
        self.serial_port.read.side_effect = exception
        self._open_transport()
        try:
            self.transport.read()
        except TransportError as ex:
            self.assertEqual(ex.message, 'se')
            self.assertEqual(ex.inner, exception)
        else:
            self.fail('Expected a TransportReadError to be raised')

    def test_read_no_data(self):
        """Test SerialTransport.read() when there is no data available.
        """
        self._create_transport()
        self.serial_port.read.return_value = b''
        self.serial_port.inWaiting.return_value = 0
        self._open_transport()
        self.assertIsNone(self.transport.read(count=3), None)

    def test_repr(self):
        """Test SerialTransport.__repr__(). Also tests equality.
        """
        self._create_transport()
        self.assertEqual(self.transport, eval(repr(self.transport)))

    def test_eq(self):
        """Test SerialTransport.__eq__() when objects are equal.
        """
        self.assertEqual(self._create_transport(), self._create_transport())

    def test_eq_same_object(self):
        """Test SerialTransport.__eq__() when comparing an object to itself.
        """
        self._create_transport()
        self.assertEqual(self.transport, self.transport)

    def test_eq_different_types(self):
        """Test SerialTransport.__eq__() when objects being compared are
        of two different types.
        """
        self.assertNotEqual(self._create_transport(), 'abc')

    def test_eq_not_equal(self):
        """Test SerialTransport.__eq__() when objects being compared are
        not equal.
        """
        self.assertNotEqual(self._create_transport(blocking=True),
                            self._create_transport(blocking=False))

if __name__ == '__main__':
    unittest.main()
