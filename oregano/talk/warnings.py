"""Common warnings.
"""

from __future__ import absolute_import

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

__all__ = [
    'TransportConfWarning'
]


class TransportConfWarning(UserWarning):
    """Generic transport configuration warning. For example, specifying non-zero
    timeouts for a non-blocking transport will result in this warning since
    the timeout values will be ignored.
    """

