"""Various test-related shortcuts and compatibility shims.
"""

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import sys
import unittest
import warnings

pyversion = '{0}.{1}'.format(*sys.version_info[:2])
if pyversion >= '3.3':
    from unittest import mock
else:
    import mock
    unittest.TestCase.assertRaisesRegex = unittest.TestCase.assertRaisesRegexp

# The following is due to iterator differences between 2.x and 3.x.
if sys.version_info[0] >= 3:

    def mock_generator(*args):
        m = mock.MagicMock()
        m.__next__.return_value = args[0]
        m.send.side_effect = args[1:]
        return m

else:

    def mock_generator(*args):
        def _iter():
            for arg in args:
                yield arg
        return _iter()
