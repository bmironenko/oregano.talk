"""General interface and common base class tests.
"""

from __future__ import absolute_import, print_function
from ..util import LoggerNameTruncatingFormatter
from ..test import *

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'


class LoggerNameTruncatingFormatterTest(unittest.TestCase):
    """Tests for the LoggerNameTruncatingFormatter class.
    """
    @mock.patch('logging.makeLogRecord')
    @mock.patch('logging.Formatter.format')
    def test_format(self, base_format, makeLogRecord):
        """Test the LoggerNameTruncatingFormatter.format() method.
        """
        formatter = LoggerNameTruncatingFormatter()
        record = mock.MagicMock()
        record.name = 'A.B.C.D'
        value = formatter.format(record)
        self.assertEqual(value, base_format.return_value)
        makeLogRecord.assert_called_once_with(record.__dict__)
        base_format.assert_called_once_with(makeLogRecord.return_value)
        self.assertEqual(makeLogRecord.return_value.name, 'D')

    @mock.patch('logging.makeLogRecord')
    def test_format_no_dots_in_name(self, makeLogRecord):
        """Test the LoggerNameTruncatingFormatter.format() method with a
        record (logger) name that does not contain any '.' characters.
        """
        record = mock.MagicMock()
        record.name = 'D'
        formatter = LoggerNameTruncatingFormatter()
        formatter.format(record)
        self.assertEqual(makeLogRecord.return_value.name, 'D')

if __name__ == '__main__':
    unittest.main()
