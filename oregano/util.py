"""Please describe me.
"""

from __future__ import absolute_import, print_function
import logging
from zope.component import IFactory, getUtility


__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'
__all__ = [
    'LoggerNameTruncatingFormatter',
    'LoggingMixin',
    'create_component',
]


class LoggerNameTruncatingFormatter(logging.Formatter):
    """Implement a custom formatter that truncates the logger name (which
    is normally the full name of the class doing the logging, packages and all)
    to the last item (class name).
    """
    def format(self, record):
        """Format a log record.

        :param record: log record to format.
        """
        # Do not modify the record passed in.
        new_record = logging.makeLogRecord(record.__dict__)
        new_record.name = record.name.split('.')[-1]
        return super(LoggerNameTruncatingFormatter, self).format(new_record)


class LoggingMixin(object):  # pragma: no cover
    """Enable logging for companion classes.
    """
    @property
    def logger(self):
        """Return the logger instance.
        """
        if not hasattr(self, '_logger'):
            if hasattr(self.__class__, '__qualname__'):
                class_name = self.__class__.__qualname__
            else:
                class_name = self.__class__.__name__
            logger_name = self.__class__.__module__ + '.' + class_name
            self._logger = logging.getLogger(logger_name)
        return self._logger


def create_component(factory_interface, *args, **config):  # pragma: no cover
    """Create a component instance from a configuration block.

    :param factory_interface: component factory interface.
    :param args: positional arguments to pass into the factory.
    :param dict config: at a mimimum, must contain the 'type' key specifying \
        the component's factory registered name.
    :return: component instance.
    """
    if not factory_interface.isOrExtends(IFactory):
        raise ValueError('\'interface\' must be an IFactory')
    if not 'type' in config:
        raise ValueError('Unspecified component type')
    factory = getUtility(factory_interface, config['type'])
    if factory:
        return factory(*args, **config.get('conf', {}))
