"""Package setup file.
"""

from __future__ import print_function, absolute_import
from setuptools import setup, find_packages
import bootstrap

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'


setup(
    name='oregano.talk',
    version=0.1,
    packages=find_packages(exclude=['*.tests']),
    setup_requires=list(bootstrap.get_dependencies('setup')),
    install_requires=list(bootstrap.get_dependencies('install')),
    test_suite='nose.collector',
    package_data={'': ['bootstrap.py', 'bootstrap.json']},
    include_package_data=True
)
