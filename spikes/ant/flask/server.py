#===============================================================================
#                             --- WARNING ---
#
#   This work contains trade secrets of DataDirect Networks, Inc.  Any
#   unauthorized use or disclosure of the work, or any part thereof, is
#   strictly prohibited.  Copyright in this work is the property of
#   DataDirect Networks.  All Rights Reserved.  In the event of publication,
#   the following notice shall apply:  Copyright 2013, DataDirect Networks, Inc.
#
#===============================================================================

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import json
import logging.config
from flask import Flask
from oregano.talk import *
from oregano.util import create_component

with open('config.json', 'r') as f:
    config = json.load(f)
logging.config.dictConfig(config['logging'])
app = Flask('flask-server')

# Create the channel.
channel = IChannel(
    create_component(
        IChannelConfFactory,
        create_component(ITransportConfFactory, **config['transport']),
        create_component(IMessageEnvelopeFactory, **config['envelope']),
        **config['channel']))


def format_bytes(data):
    """Format a byte buffer.
    """
    return ' '.join(['{0:x}'.format(byte) for byte in data])


@app.route('/api/reset', methods=['GET'])
def reset():
    """Reset the ANT stick.
    """
    channel.send(b'\x4A\x00')
    return format_bytes(channel.receive())


if __name__ == "__main__":
    app.run(**config['server'])

