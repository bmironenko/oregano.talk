#===============================================================================
#                             --- WARNING ---
#
#   This work contains trade secrets of DataDirect Networks, Inc.  Any
#   unauthorized use or disclosure of the work, or any part thereof, is
#   strictly prohibited.  Copyright in this work is the property of
#   DataDirect Networks.  All Rights Reserved.  In the event of publication,
#   the following notice shall apply:  Copyright 2013, DataDirect Networks, Inc.
#
#===============================================================================

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import json
import logging.config
from oregano.talk import *
from oregano.util import create_component


def format_bytes(data):
    """Format a byte buffer.
    """
    return ' '.join(['{0:x}'.format(byte) for byte in data])


def main():
    with open('config.json', 'r') as f:
        config = json.load(f)
    logging.config.dictConfig(config['logging'])

    # Create the channel.
    channel = IChannel(
        create_component(
            IChannelConfFactory,
            create_component(ITransportConfFactory, **config['transport']),
            create_component(IMessageEnvelopeFactory, **config['envelope']),
            **config['channel']))

    channel.send(b'\x4A\x00')
    while True:
        print(format_bytes(channel.receive()))


if __name__ == "__main__":
    main()
