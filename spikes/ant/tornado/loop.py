"""Example Tornado server.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import json
import logging.config
from tornado.ioloop import IOLoop
from tornado.gen import coroutine
from oregano.talk import *
from oregano.util import create_component


def format_bytes(data):
    """Format a byte buffer.
    """
    return ' '.join(['{0:x}'.format(byte) for byte in data])


@coroutine
def run(channel):
    """Run the event loop.
    """
    logger = logging.getLogger('run')
    yield channel.send(b'\x4A\x00')
    while True:
        message = yield channel.receive()
        # TODO: Parse the message and update the state.
        logger.info(format_bytes(message) + '\n')


if __name__ == "__main__":

    # Read the configuration file.
    with open('config.json', 'r') as f:
        config = json.load(f)

    # Configure logging.
    logging.config.dictConfig(config['logging'])

    # Create the channel.
    channel = IChannel(
        create_component(
            IChannelConfFactory,
            create_component(ITransportConfFactory, **config['transport']),
            create_component(IMessageEnvelopeFactory, **config['envelope']),
            **config['channel']))

    loop = IOLoop.instance()
    loop.add_callback(run, channel)
    loop.start()
