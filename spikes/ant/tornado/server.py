"""Example Tornado server.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import json
import logging.config
from tornado.ioloop import IOLoop
from tornado.gen import coroutine
from tornado.web import RequestHandler, Application
from oregano.talk import *
from oregano.shims import *
from oregano.util import create_component


def format_bytes(data):
    """Format a byte buffer.
    """
    return ' '.join(['{0:x}'.format(byte) for byte in data])


class ANTResetHandler(RequestHandler):

    def initialize(self):
        self.channel = self.settings['channel']

    @coroutine
    def get(self):
        yield self.channel.send(b'\x4A\x00')
        response = yield self.channel.receive()
        self.write(format_bytes(response) + '\n')


if __name__ == "__main__":

    # Read the configuration file.
    with open('config.json', 'r') as f:
        config = json.load(f)

    # Configure logging.
    logging.config.dictConfig(config['logging'])

    # Create the channel.
    channel = IChannel(
        create_component(
            IChannelConfFactory,
            create_component(ITransportConfFactory, **config['transport']),
            create_component(IMessageEnvelopeFactory, **config['envelope']),
            **config['channel']))

    application = Application([
        (r'/api/reset', ANTResetHandler),
    ], channel=channel)

    application.listen(config['server']['port'])
    IOLoop.instance().start()
