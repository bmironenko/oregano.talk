"""Example Tornado server.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import json
import logging.config
import tornado.ioloop
import tornado.web
import tornado.concurrent
from tornado import gen
from oregano.shims import *
from oregano.talk import *
from oregano.util import create_component


class EchoHandler(tornado.web.RequestHandler):

    def initialize(self):
        self.channel = self.settings['channel']

    @gen.coroutine
    def get(self, data):
        converted = b(data, 'UTF-8')
        yield self.channel.send(converted)
        response = yield self.channel.receive()
        self.write(response)


if __name__ == "__main__":

    # Read the configuration file.
    with open('config.json', 'r') as f:
        config = json.load(f)

    # Configure logging.
    logging.config.dictConfig(config['logging'])

    # Create the channel.
    channel = IChannel(
        create_component(
            IChannelConfFactory,
            create_component(ITransportConfFactory, **config['transport']),
            create_component(IMessageEnvelopeFactory, **config['envelope']),
            **config['channel']))

    application = tornado.web.Application([
        (r'/api/echo/([A-Za-z]+)', EchoHandler),
    ], channel=channel)

    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
