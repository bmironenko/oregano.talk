#include "Arduino.h"

#define BAUD_RATE 9600
#define READ_TIMEOUT 2000
#define SENTINEL "\x02\x03"

/** Setup.
 */
void setup() {
  Serial.setTimeout(READ_TIMEOUT);
  Serial.begin(BAUD_RATE); 
}

/** Main program loop.
 */
void loop() {
  process_message();
  delay(200);
}

void process_message() {
  static char read_buffer[256];
  int message_size = 0;
  int i = 0;
  
  if (Serial.available() > 0 && Serial.find(SENTINEL)) {

    do {
      
      // Read message size. 
      if (Serial.readBytes(read_buffer, 2)) {
        message_size = read_buffer[0];
      } else {
        // timeout
        break;
      }

      // Check message size.
      if (message_size > sizeof(read_buffer)) {
        // message too big
        break;
      }
      
      // Read the payload. 
      if (message_size > 0 && Serial.readBytes(read_buffer, (size_t)message_size) < message_size) {
        // timeout
        break;
      }
      
      // Modify the payload.
      for (i = 0; i < message_size; i++) {
        read_buffer[i] += 2;
      }
      
      // Respond.
      Serial.print(SENTINEL);
      Serial.write(message_size);
      Serial.write(message_size >> 8);
      Serial.write((const uint8_t*)&read_buffer, message_size);
    } while (false);
  }
}
