"""Tornado version of Hello, World.
"""

from __future__ import absolute_import, print_function

__author__ = 'Basil Mironenko'
__email__ = 'bmironenko@me.com'

import tornado.ioloop
import tornado.web


class MainHandler(tornado.web.RequestHandler):
    def initialize(self, **kwargs):
        self.message = kwargs.get('message',
                                  self.settings.get('default_message', ''))

    def get(self):
        self.write(self.message)

application = tornado.web.Application([
    (r"/", MainHandler, dict(message='Hello, world')),
], default_message='Default message')

if __name__ == "__main__":
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
